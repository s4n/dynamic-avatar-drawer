import {DecorativePart} from "./decorative_part";
import {Layer} from "../util/canvas";
import {none, adjust} from "drawpoint/dist-esm";
import {Materials} from "../materials";

export class LegFur extends DecorativePart {
    constructor(...data) {
        super(Materials.brownFur, {
            fill      : none,
            loc       : "+leg",
            layer     : Layer.FRONT,
            aboveParts: ["parts leg"]
        }, ...data);
    }

    calcDrawPoints(ex) {
        const points = [];
        // fur near the "feet"
        // TODO implement (drawCurl no longer exists)
        // points.extend(drawCurl(adjust(ex.ankle.in, -1.5, 0), 5, 10, 0.3, 0.7));
        // points.extend(drawCurl(adjust(ex.ankle.inbot, 4, -1), 6, 8, 0.55, 0, 0.7));
        // points.extend(drawCurl(adjust(ex.knee.in, -1.3, 1), 4, 9, 0.4, 0.6, 0.6));
        // points.extend(
        //     drawCurl(adjust(ex.calf.out, 0.7, -1), 5, 12, 0.7, -0.5, -0.35));
        // points.extend(
        //     drawCurl(adjust(ex.thigh.out, 0.2, -3), 6, 10, 0.7, -0.5, -0.35));

        return points;
    }
}
