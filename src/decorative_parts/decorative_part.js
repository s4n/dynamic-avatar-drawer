import {Layer} from "../util/canvas";
import {BasePart} from "../parts/part";

/**
 * Base class for non-combat parts for show
 * @memberof module:da
 */
export class DecorativePart extends BasePart {
    constructor(...data) {
        super({
            loc         : null,
            layer       : Layer.BASE,
            reflect     : false,
            coverConceal: [],
            uncoverable : false,
        }, ...data);
    }

    stroke() {
        return "inherit";
    }

    fill() {
        return "inherit";
    }

    // how thick the stroke line should be
    getLineWidth() {
        return 1.5;
    }
}

