import {FacePart} from "./face_part";
import {adjustColor} from "../util/utility";
import {Layer} from "../util/canvas";
import {extractPoint, clamp, splitCurve, simpleQuadratic} from "drawpoint/dist-esm";

class Ears extends FacePart {
    constructor(...data) {
        super({
            loc       : "ears",
            layer     : Layer.BELOW_HAIR,
            belowParts: ["parts head"],
        }, ...data);
    }

    clipFill() {
    }

    clipStroke() {
    }

    fill(ignore, ex) {
        return adjustColor(ex.baseFill,
            {
                s: -18,
                l: -2
            });
    }

    getLineWidth(avatar) {
        return clamp(1.7 - avatar.dim.faceFem * 0.05, 0.7, 1.5);
    }
}


export class EarsHuman extends Ears {
    constructor(...data) {
        super(...data);
    }

    calcDrawPoints(ex, mods, calculate) {
        if (calculate) {
            const ear = ex.ear = {};
            ear.top = extractPoint(ex.skull.side);
            ear.bot = extractPoint(ex.skull.bot);
            ear.mid = {
                x: ear.top.x + 0.6,
                y: ear.top.y * 0.4 + ear.bot.y * 0.6
            };
            ear.mid.cp1 = {
                x: ear.top.x + 1.5,
                y: ear.top.y + 0.5
            };
            ear.mid.cp2 = {
                x: ear.mid.x + 0.5,
                y: ear.mid.y + 0.9
            };
            ear.bot.cp1 = {
                x: ear.mid.x,
                y: ear.mid.y - 0.7
            };
            ear.bot.cp2 = {
                x: ear.bot.x + 0.6,
                y: ear.bot.y - 0.1
            };
        }
        return [ex.ear.top, ex.ear.mid, ex.ear.bot];
    }
}


export class EarsElf extends Ears {
    constructor(...data) {
        super(...data);
    }

    calcDrawPoints(ex, mods, calculate) {
        if (calculate) {
            const ear = ex.ear = {};
            ear.top = extractPoint(ex.skull.side);
            const sp = splitCurve(0.3, ex.skull.side, ex.skull.bot);
            ear.bot = sp.left.p2;
            ear.mid = {
                x: ear.top.x + 6 + mods.earlobeLength,
                y: ear.top.y + 0.5
            };
            ear.mid.cp1 = simpleQuadratic(ear.top, ear.mid, 0.30, 1);
            ear.bot.cp1 = {
                x: ear.mid.x - 2,
                y: ear.mid.y - 2
            };
            ear.bot.cp2 = {
                x: ear.bot.x + 2,
                y: ear.bot.y + 4
            };

        }
        return [ex.ear.top, ex.ear.mid, ex.ear.bot];
    }
}

