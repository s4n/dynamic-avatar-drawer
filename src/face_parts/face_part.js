import {none} from "drawpoint/dist-esm";
import {Layer} from "../util/canvas";
import {BasePart} from "../parts/part";

/**
 * Base class for parts that are on the face (head)
 * @memberof module:da
 */
export class FacePart extends BasePart {
    constructor(...data) {
        super({
            layer       : Layer.BELOW_HAIR,
            reflect     : false,
            coverConceal: [],
            uncoverable : false,
        }, ...data);
    }

    stroke() {
        return none;
    }

    fill() {
        return "inherit";
    }

    // how thick the stroke line should be
    getLineWidth() {
        return 1.5;
    }
}

