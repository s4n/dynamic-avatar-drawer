import {FacePart} from "./face_part";
import {
    extractPoint,
    adjust,
} from "drawpoint/dist-esm";

class Eyelid extends FacePart {
    constructor(...data) {
        super({
            loc       : "eyelid",
            aboveParts: ["eyes", "iris", "pupil"],
        }, ...data);
    }
}


export class EyelidHuman extends Eyelid {
    constructor(...data) {
        super(...data);
    }

    fill(ignore, ex) {
        return ex.eyelidColor;
    }


    calcDrawPoints(ex, mods, calculate) {
        if (calculate) {
            ex.eyelid = {};
            let top = adjust(ex.eyes.top, mods.eyelidBias * 0.1,
                -0.3 + mods.eyelidHeight * 0.1);
            let out = adjust(ex.eyes.out, mods.eyelidBias * 0.1,
                -0.3 + mods.eyelidHeight * 0.1);

            ex.eyelid.top = {
                x: top.x,
                y: top.y
            };
            ex.eyelid.top.cp1 = out.cp2;
            ex.eyelid.top.cp2 = out.cp1;

            ex.eyelid.in = extractPoint(ex.eyes.in);
            // need to invert since originally the point is from eyes.in to eyes.top
            ex.eyelid.in.cp1 = top.cp2;
            ex.eyelid.in.cp2 = top.cp1;
            if (mods.eyelidHeight > 5) {
                ex.eyelid.in.cp1.x -= mods.eyelidHeight * 0.02;
                ex.eyelid.in.cp2.x -= mods.eyelidHeight * 0.02;
            }
        }

        return [ex.eyes.in, ex.eyes.top, ex.eyes.out, ex.eyelid.top, ex.eyelid.in];
    }

}

