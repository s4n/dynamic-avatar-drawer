import {HairPart, Hair} from "./hair_part";
import {
    adjust,
    splitCurve,
    simpleQuadratic,
    drawPoints,
    extractPoint,
    reflect, clamp, drawSpecificCurl,
} from "drawpoint/dist-esm";
import {averagePoint} from "../util/utility";
import {ShadingPart} from "../draw/shading_part";
import {seamWidth, Layer} from "../util/canvas";

class HimeCutFrontShading extends ShadingPart {
    constructor(...data) {
        super({
            loc  : "+head",
            layer: Layer.BELOW_HAIR,
        }, ...data);
    }

    calcDrawPoints(ex) {
        let {
            left,
            right,
            top
        } = calcHimeCut(ex, clamp(this.hairLength, 0, 53));

        left = adjust(left, 0, -1);
        right = adjust(right, 0, -1);

        return [right, left, top, right];
    }
}


export class HimeCutBack extends HairPart {
    constructor(...data) {
        super(Hair.hairBack, {
            reflect: true,
        }, ...data);
    }

    renderHairPoints(ctx, ex) {
        const hl = this.hairLength;
        let {right, top} = calcHimeCut(ex, clamp(hl, 0, 53));
        top.y += hl * 0.01;
        top.x -= seamWidth;
        right = adjust(right, hl * 0.005, 0);
        right.cp1.y += hl * 0.01;

        const rightBot = {
            x: right.x,
            y: right.y - hl * 0.8
        };
        const bot = {
            x: -seamWidth,
            y: rightBot.y - hl * 0.01
        };

        ctx.beginPath();
        drawPoints(ctx, top, right, rightBot, bot);
        ctx.fill();
        ctx.stroke();
    }
}


export class HimeCutFront extends HairPart {
    constructor(...data) {
        super(Hair.hairFront, {
            shadingParts: [HimeCutFrontShading],
        }, ...data);
    }

    renderHairPoints(ctx, ex) {
        const hl = clamp(this.hairLength, 0, 53);
        const {left, right, top} = calcHimeCut(ex, hl);

        // first draw the bang (don't want to stroke the top as well so we'll fill later
        ctx.beginPath();
        drawPoints(ctx, right);
        // gaps in the hair
        {
            let sp = splitCurve(0.2, right, left);
            const gapRight = sp.left.p2;
            gapRight.deflection = -2;

            sp = splitCurve(0.221, right, left);
            left.cp1 = sp.left.p2.cp1;

            const [, gapTop, gapLeft] = drawSpecificCurl(
                gapRight,
                averagePoint(gapRight, top, 0.6),
                {
                    x         : sp.left.p2.x,
                    y         : sp.left.p2.y,
                    deflection: -2
                });
            drawPoints(ctx, null, gapRight, gapTop, gapLeft);
        }

        {
            let sp = splitCurve(0.6, right, left);
            const gapRight = sp.left.p2;
            gapRight.deflection = -1;

            sp = splitCurve(0.615, right, left);
            left.cp1 = sp.left.p2.cp1;

            const [, gapTop, gapLeft] = drawSpecificCurl(
                gapRight,
                averagePoint(gapRight, top, 0.4),
                {
                    x         : sp.left.p2.x,
                    y         : sp.left.p2.y,
                    deflection: -1
                });
            drawPoints(ctx, null, gapRight, gapTop, gapLeft);
        }

        {
            let sp = splitCurve(0.9, right, left);
            const gapRight = sp.left.p2;
            gapRight.deflection = 4;

            sp = splitCurve(0.91, right, left);
            left.cp1 = sp.left.p2.cp1;

            const [, gapTop, gapLeft] = drawSpecificCurl(
                gapRight,
                averagePoint(gapRight, top, 0.5),
                {
                    x         : sp.left.p2.x,
                    y         : sp.left.p2.y,
                    deflection: 4
                });
            drawPoints(ctx, null, gapRight, gapTop, gapLeft);
        }

        // closing up to the left
        drawPoints(ctx, null, left, top, right);
        ctx.fill();
    }
}


export class HimeCutSide extends HairPart {
    constructor(...data) {
        super(Hair.hairFront, {
            loc       : "+front hair",
            aboveParts: ["hairParts hair"],
            reflect   : true
        }, ...data);
    }

    renderHairPoints(ctx, ex) {
        const hl = this.hairLength;
        if (hl < 40) {
            return;
        }
        let {left, right} = calcHimeCut(ex, clamp(hl, 0, 53));
        right = adjust(right, 0.4, 0.9);
        right.cp1.y += hl * 0.01;

        const sp = splitCurve(0.1, right, left);
        const inner = sp.left.p2;

        const bot = {
            x: right.x,
            y: right.y - (hl - 40) * 0.4
        };
        const innerBot = {
            x: inner.x,
            y: bot.y - 1
        };
        innerBot.cp1 = simpleQuadratic(bot, innerBot, 0.5, 1);

        // first draw the bang (don't want to stroke the top as well so we'll fill later
        ctx.beginPath();
        drawPoints(ctx, right, bot, innerBot, inner);
        ctx.fill();
    }
}


export function calcHimeCut(ex, hl) {
    const sp = splitCurve(0.4 + hl / 82, ex.skull, ex.skull.side);
    // skull down to right
    let right = sp.left.p2;
    right = adjust(right, 1, hl / 100);
    right.cp1.x -= hl * 0.09;

    // curve down in front
    const left = reflect(right);
    left.cp1 = simpleQuadratic(right, left, 0.5, hl / 20);

    let top = extractPoint(ex.skull);
    top.cp1 = reflect(right.cp2);
    top.cp2 = reflect(right.cp1);
    top = adjust(top, 0, 2);
    right.cp1.y += 2;
    right.cp2.y += 2;
    return {
        left,
        right,
        top
    };
}
