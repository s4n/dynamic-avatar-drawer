import {Layer} from "../util/canvas";
import {BasePart, getSideValue} from "../parts/part";

/**
 * Base class for all hair parts; should go in a Player's hairParts
 * @memberof module:da
 */
export class HairPart extends BasePart {
    constructor(...data) {
        super({
            layer       : Layer.HAIR,
            reflect     : false,
            coverConceal: [],
            uncoverable : false,
        }, ...data);
    }

    stroke(ignore, ex) {
        return ex.hairStroke;
    }

    fill(ignore, ex) {
        return ex.hairFill;
    }

    getLineWidth() {
        return 1.5;
    }
}


/**
 * Where all hairs go
 * @namespace Hair
 * @memberof module:da
 */
export const Hair = {

    /**
     * Create a HairPart instance
     * @memberof module:da.Hair
     * @param {module:da.HairPart} Part Prototype of hair part
     * @param {object} data Overriding data for this particular part
     * @returns {module:da.HairPart} Instantiated hair part
     */
    create(Part, ...data) {
        let part = new Part(...data);

        // could potentially provided override in data
        if (part.hasOwnProperty("side") === false) {
            part.side = getSideValue(null);
        }

        return part;
    },
    hairBack     : Object.freeze({
        loc       : "back hair",
        layer     : Layer.BACK,
        belowParts: ["parts groin"],
    }),
    hairAboveEars:
        Object.freeze({
            loc       : "ears hair",
            layer     : Layer.BELOW_HAIR,
            aboveParts: ["ears"],
        }),
    hairMedium   :
        Object.freeze({
            loc       : "medium hair",
            layer     : Layer.GENITALS,
            aboveParts: ["chest", "neck"],
        }),
    hairFront    :
        Object.freeze({
            loc  : "front hair",
            layer: Layer.HAIR,
        }),
};
