"use strict";
// all the polyfills
if (typeof Object.assign != "function") {
    (function () {
        Object.assign = function (target) {
            "use strict";
            // We must check against these specific cases.
            if (target === undefined || target === null) {
                throw new TypeError("Cannot convert undefined or null to object");
            }

            const output = Object(target);
            for (let index = 1; index < arguments.length; index++) {
                const source = arguments[index];
                if (source !== undefined && source !== null) {
                    for (let nextKey in source) {
                        if (source.hasOwnProperty(nextKey)) {
                            output[nextKey] = source[nextKey];
                        }
                    }
                }
            }
            return output;
        };
    })();
}
String.prototype.capitalizeFirstLetter = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
};
if (!Array.prototype.last) {
    Array.prototype.last = function () {
        return (this.length) ? this[this.length - 1] : null;
    };
}
if (!Array.prototype.extend) {
    Array.prototype.extend = function (b) {
        this.push.apply(this, b);
    };
}
Array.prototype.contains = function (needle) {
    const findNaN = needle !== needle;
    let indexOf;

    if (!findNaN && typeof Array.prototype.indexOf === "function") {
        indexOf = Array.prototype.indexOf;
    } else {
        indexOf = function (needle) {
            var i = -1, index = -1;

            for (i = 0; i < this.length; i++) {
                var item = this[i];

                if ((findNaN && item !== item) || item === needle) {
                    index = i;
                    break;
                }
            }

            return index;
        };
    }

    return indexOf.call(this, needle) > -1;
};

if (!String.prototype.startsWith) {
    String.prototype.startsWith = function (searchString, position) {
        position = position || 0;
        return this.substr(position, searchString.length) === searchString;
    };
}

/**
 * Extract numeric RGB values from a HTML compatible string (whitespace ignored)
 * @memberof module:da
 * @param {string} rgbString RGB string in the format "rgb(100,220,42)"
 * @returns {(object|null)} Either an object holding r,g,b properties, or null if not matched
 */
export function extractRGB(rgbString) {
    const rgb = /rgb\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*\)/.exec(rgbString);
    if (rgb) {
        return {
            r: parseInt(rgb[1]),
            g: parseInt(rgb[2]),
            b: parseInt(rgb[3])
        };
    }
    return null;
}

/**
 * Extract numeric HSL values from a HTML compatible string (whitespace ignored)
 * @memberof module:da
 * @param {string} hslString HSL string in the format "hsl(310,12%,25%)"
 * @returns {(object|null)} Either an object holding h,s,l properties, or null if not matched
 */
export function extractHSL(hslString) {
    const hsl = /hsl\(\s*(\d+)\s*,\s*(\d+)%\s*,\s*(\d+)%\s*\)/.exec(hslString);
    if (hsl) {
        return {
            h: parseInt(hsl[1]),
            s: parseInt(hsl[2]),
            l: parseInt(hsl[3]),
        };
    }
    return null;
}

/**
 * Extract numeric RGB values from a HTML compatible hex string (whitespace ignored)
 * @memberof module:da
 * @param {string} hexString Hex string in the format "#ffaabc"
 * @returns {(object|null)} Either an object holding r,g,b properties, or null if not matched
 */
export function extractHex(hexString) {
    const rgb = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hexString);
    if (rgb) {
        return {
            r: parseInt(rgb[1], 16),
            g: parseInt(rgb[2], 16),
            b: parseInt(rgb[3], 16)
        };
    }
    return null;
}

/**
 * Convert an RGB object to HSL object, which are more intuitive to modify.
 * Adapted from https://github.com/mjackson/
 * @param {object} rgb RGB object holding r,g,b properties (each [0,255])
 * @returns {object} HSL object holding h,s,l properties (h [0,360], s,l [0,100])
 */
export function RGBToHSL(rgb) {
    let r, g, b;
    [r, g, b] = [rgb.r, rgb.g, rgb.b];
    r /= 255, g /= 255, b /= 255;
    const max = Math.max(r, g, b), min = Math.min(r, g, b);
    let h, s, l = (max + min) / 2;

    if (max === min) {
        h = s = 0;  // achromatic
    } else {
        const d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
        switch (max) {
        case r:
            h = (g - b) / d + (g < b ? 6 : 0);
            break;
        case g:
            h = (b - r) / d + 2;
            break;
        case b:
            h = (r - g) / d + 4;
            break;
        default:
            break;
        }
        h /= 6;
    }
    h *= 360;
    s *= 100;
    l *= 100;

    return rgb.hasOwnProperty("a") ? {
        h,
        s,
        l,
        a: rgb.a
    } : {
        h,
        s,
        l
    };
}

/**
 * Adjust an existing color into a new color
 * @memberof module:da
 * @param color A color in RGB, hex, or HSL form
 * @param adjustment Object with H, S, L, and optionally A as properties
 */
export function adjustColor(color, adjustment) {
    // convert everything to HSL
    let hsl = null;
    if (typeof color === "string") {
        // get the first non-null result
        hsl = extractHSL(color);
        if (hsl === null) {
            hsl = hsl || extractRGB(color);
            hsl = hsl || extractHex(color);
            // have an RGB value
            if (hsl) {
                hsl = RGBToHSL(hsl);
            }
        }
    } else if (color.hasOwnProperty("h") && color.hasOwnProperty("s") &&
               color.hasOwnProperty("l")) {
        hsl = color;
    } else if (color.hasOwnProperty("r") && color.hasOwnProperty("g") &&
               color.hasOwnProperty("b")) {
        hsl = RGBToHSL(color);
    }
    // can't do it
    if (hsl === null) {
        return null;
    }
    hsl.h += adjustment.h || 0;
    hsl.s += adjustment.s || 0;
    hsl.l += adjustment.l || 0;
    if (adjustment.hasOwnProperty("a")) {
        return `hsla(${hsl.h.toFixed(1)},${hsl.s.toFixed(1)}%,${hsl.l.toFixed(1)}%,${adjustment.a.toFixed(
            2)})`;
    } else {
        return `hsl(${hsl.h.toFixed(1)},${hsl.s.toFixed(1)}%,${hsl.l.toFixed(1)}%)`;
    }
}

function RLE(s) {
    // simple run length encoding for images (4 characters at a time)
    let shortests = String.fromCharCode(0) + s;

    // try encoding multiples of 4 (repetitions could be at different intervals)
    // try subpixel encoding first ()
    for (let j = 1; j < 41;) {
        // first character to indicate how many pixels is a pattern
        let ss = String.fromCharCode(j);
        let c = s.slice(0, j);
        let repeat = 1;

        let i = j;
        for (var len = s.length; i < len; i += j) {
            // base64 can only handle up to 255
            if (c !== s.slice(i, i + j) || repeat === 255) {
                ss += String.fromCharCode(repeat) + c;
                repeat = 1;
                c = s.slice(i, i + j);
            } else {
                ++repeat;
            }
        }
        // was still repeating a sequence at the end
        if (c === s.slice(s.length - j)) {
            ss += String.fromCharCode(repeat) + c;
        }// copy over the end (number of total pixels doesn't divide 4*stride evenly)
        else if (i > s.length - 1) {
            ss += String.fromCharCode(1) + s.slice(i - j);
        }

        console.log("RLE over", j, "stride -- compressed", ss.length, "original", s.length);
        if (ss.length < shortests.length) {
            shortests = ss;
        }
        // after subpixel encoding, increment by a pixel
        if (j === 1) {
            j = 4;
        } else {
            j += 4;
        }
    }
    // actually smaller to keep original string
    return shortests;
}

function RLD(ss) {
    // simple run length decoding
    const stride = ss.charCodeAt(0);
    if (stride === 0) {
        return ss.slice(1);
    }

    let s = "";
    for (let i = 1, len = ss.length; i < len; i += 1 + stride) {
        s += ss.slice(i + 1, i + 1 + stride).repeat(ss.charCodeAt(i));
    }
    console.log("decoded length", s.length);
    return s;
}

// zigurat algorithm, from
// https://www.filosophy.org/post/35/normaldistributed_random_values_in_javascript_using_the_ziggurat_algorithm/
function Ziggurat() {
    let jsr = 123456789;
    let wn = Array(128);
    let fn = Array(128);
    let kn = Array(128);

    function RNOR() {
        const hz = SHR3();
        const iz = hz & 127;
        return (Math.abs(hz) < kn[iz]) ? hz * wn[iz] : nfix(hz, iz);
    }

    this.nextGaussian = function () {
        return RNOR();
    };

    function nfix(hz, iz) {
        const r = 3.442619855899;
        const r1 = 1.0 / r;
        let x;
        let y;
        while (true) {
            x = hz * wn[iz];
            if (iz == 0) {
                x = (-Math.log(UNI()) * r1);
                y = -Math.log(UNI());
                while (y + y < x * x) {
                    x = (-Math.log(UNI()) * r1);
                    y = -Math.log(UNI());
                }
                return (hz > 0) ? r + x : -r - x;
            }

            if (fn[iz] + UNI() * (fn[iz - 1] - fn[iz]) < Math.exp(-0.5 * x * x)) {
                return x;
            }
            hz = SHR3();
            iz = hz & 127;

            if (Math.abs(hz) < kn[iz]) {
                return (hz * wn[iz]);
            }
        }
    }

    function SHR3() {
        const jz = jsr;
        let jzr = jsr;
        jzr ^= (jzr << 13);
        jzr ^= (jzr >>> 17);
        jzr ^= (jzr << 5);
        jsr = jzr;
        return (jz + jzr) | 0;
    }

    function UNI() {
        return 0.5 * (1 + SHR3() / -Math.pow(2, 31));
    }

    function zigset() {
        // seed generator based on current time
        jsr ^= new Date().getTime();

        const m1 = 2147483648.0;
        let dn = 3.442619855899;
        let tn = dn;
        const vn = 9.91256303526217e-3;

        var q = vn / Math.exp(-0.5 * dn * dn);
        kn[0] = Math.floor((dn / q) * m1);
        kn[1] = 0;

        wn[0] = q / m1;
        wn[127] = dn / m1;

        fn[0] = 1.0;
        fn[127] = Math.exp(-0.5 * dn * dn);

        for (var i = 126; i >= 1; i--) {
            dn = Math.sqrt(-2.0 * Math.log(vn / dn + Math.exp(-0.5 * dn * dn)));
            kn[i + 1] = Math.floor((dn / tn) * m1);
            tn = dn;
            fn[i] = Math.exp(-0.5 * dn * dn);
            wn[i] = dn / m1;
        }
    }

    zigset();
}

// instantiate the generator
let randZig;

/**
 * Get a normally distributed random number
 * @memberof module:da
 * (using the Ziggurat algorithm https://en.wikipedia.org/wiki/Ziggurat_algorithm)
 * @param {number} mean Mean of the underlying normal distribution
 * @param {number} stdev Standard deviation around mean
 * @returns {number} The random number
 */
export function randNormal(mean, stdev) {
    // pseudo random approximate
    // standard normal: mean 0 and stdev 1
    if (!randZig) {
        randZig = new Ziggurat();
    }
    return randZig.nextGaussian() * stdev + mean;
}

export function testRandGenerator(n) {
    const histogram =
        {};  // buckets with each key being an integer mapping to the number of occurances
    for (let i = -100; i < 101; ++i) {
        histogram[i] = 0;
    }

    let v;
    while (n-- > 0) {
        v = Math.round(randNormal(10, 20));
        ++histogram[v];
    }

    let str = [];
    for (let i in histogram) {
        str.push("" + i + "\t" + histogram[i]);
    }
    console.log(str.join("\n"));
}


export function averagePoint(p1, p2, bias = 0.5) {
    return {
        x: p1.x * (1 - bias) + p2.x * bias,
        y: p1.y * (1 - bias) + p2.y * bias
    };
}


// topological sort from https://github.com/marcelklehr/toposort
export function topologicalSort(nodes, edges) {
    let cursor = nodes.length
        , sorted = new Array(cursor)
        , visited = {}
        , i = cursor;

    while (i--) {
        if (!visited[i]) {
            visit(nodes[i], i, []);
        }
    }

    return sorted;

    function visit(node, i, predecessors) {
        if (predecessors.indexOf(node) >= 0) {
            throw new Error("Cyclic dependency: " + JSON.stringify(node));
        }

        if (!~nodes.indexOf(node)) {
            throw new Error("Found unknown node. Make sure to provided all involved nodes. Unknown node: " +
                            JSON.stringify(node));
        }

        if (visited[i]) {
            return;
        }
        visited[i] = true;

        // outgoing edges
        const outgoing = edges.filter(function (edge) {
            return edge[0] === node;
        });

        if ((i = outgoing.length)) {
            const preds = predecessors.concat(node);
            do {
                const child = outgoing[--i][1];
                visit(child, nodes.indexOf(child), preds);
            } while (i);
        }

        sorted[--cursor] = node;
    }
}

export function simpleTopologicalSort(edges) {
    return topologicalSort(uniqueNodes(edges), edges);
}

function uniqueNodes(arr) {
    const res = [];
    for (let i = 0, len = arr.length; i < len; i++) {
        let edge = arr[i];
        if (res.indexOf(edge[0]) < 0) {
            res.push(edge[0]);
        }
        if (res.indexOf(edge[1]) < 0) {
            res.push(edge[1]);
        }
    }
    return res;
}

