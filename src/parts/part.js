import {
    extractSideLocation,
    extractBaseLocation,
    extractLocationModifier,
    extractUnmodifiedLocation
} from "../util/part";
import {none} from "drawpoint/dist-esm";
import {Layer} from "../util/canvas";

/**
 * All parts should go in this namespace
 * @namespace Part
 * @memberof module:da
 */

export class BasePart {
    // TODO add shared data here
    constructor(...data) {
        Object.assign(this, ...data);
    }
}

export class BodyPart extends BasePart {
    constructor(...data) {
        /**
         * The default properties of all body parts; add to this object to add additional default properties
         * @property {(string|null)} loc Location of the part, prepend with + to mean allow other
         * parts to also occupy this location, - to mean restrict any other part from being here
         * @property {module:da.Layer} layer Drawing layer,
         * @property {boolean} reflect Whether this part should be drawn on the other side as well
         * @property {string[]} coverConceal Whether this part should not be drawn if there are clothes
         * covering its location
         * @property {boolean} uncoverable Whether it's possible to wear anything over this part
         * (mutually exclusive with converconceal)
         * @property {string[]} aboveParts List of "{part group} {location}" strings that specify
         * which parts of which part groups this part should be drawn above. For example, having
         * "decorativeParts torso" will ensure this part gets drawn above any parts in that location.
         * Specifying only a location will mean to be above any part in that location regardless of group.
         * @property {string[]} belowParts Opposite to aboveParts.
         */
        super( {
            loc         : null,
            parentPart  : null,
            layer       : Layer.BASE,
            reflect     : false,
            coverConceal: [],
            uncoverable : false,
            aboveParts  : [],
            belowParts  : [],
        }, ...data);
    }

    /**
     * Set the stroke pattern for this part
     * @returns {string}
     */
    stroke() {
        return none;
    }

    /**
     * Set the fill pattern for this part
     * @returns {string}
     */
    fill() {
        return "inherit";
    }

    /**
     * Set how thick the stroke line should be
     * @returns {number}
     */
    getLineWidth() {
        return 0.8;
    }

    /**
     * Calculate drawpoints associated with this part and return it in the sequence to be drawn.
     * @this {object} Calculated dimensions of the player owning the part
     * @param {object} ex Exports from draw that should hold draw points calculated up to now;
     * additional draw points defined by this part should be defined on ex
     * @param {object} mods Combined modifiers of the part and the Player owning the part
     * @param {boolean} calculate
     * @param {module:da.BodyPart} part The body part itself
     * @return {object[]} List of draw points (or convertible to draw point objects)
     */
    calcDrawPoints(ex, mods, calculate, part) {
        /*eslint no-unused-vars: ["off", { "args": "all" }]*/
    }
}


export const Part = {

    /**
     * Right side of the body for anything taking side
     * @readonly
     * @type {number}
     */
    RIGHT: 0,
    /**
     * Left side of the body for anything taking side
     * @readonly
     * @type {number}
     */
    LEFT : 1,
    /**
     * Give me a base body part and a side it's supposed to be on
     * I'll return to you a body part specific to that side
     * @memberof module:da.Part
     * @param {BodyPart} PartPrototype Prototype to instantiate with
     * @param {...object} userData Overriding data
     * @returns {BodyPart}
     */
    create(PartPrototype, ...userData) {
        const data = Object.assign({}, ...userData);
        let part = new PartPrototype(data);

        let side = (data && data.hasOwnProperty("side")) ? data.side : null;

        // override if part location specifies side
        if (side === null) {
            side = extractSideLocation(part.loc);
        }

        // direct override
        if (part.forcedSide !== undefined) {
            side = part.forcedSide;
        }

        const sideString = getSideLocation(side);

        // rename for more specificity
        if (sideString === "right" || sideString === "left") {
            const baseLocation = extractBaseLocation(part.loc);
            const locationModifiers = extractLocationModifier(part.loc);
            part.loc = locationModifiers + sideString + " " + baseLocation;
            if (part.parentPart) {
                part.parentPart = sideString + " " + part.parentPart;
            }
        }

        // configure side to be a standard format
        part.side = getSideValue(side);

        return part;
    },
};


/**
 * Get a side string in a well defined format
 * @param side A side string
 * @returns {(string|null)} Either the side string or null if unrecognized
 */
export function getSideLocation(side) {
    if (side === "right" || side === "left") {
        return side;
    }
    if (side === Part.LEFT) {
        return "left";
    }
    if (side === Part.RIGHT) {
        return "right";
    }
    return null;
}


/**
 * Check whether two parts conflict
 * @param {Part} partA
 * @param {Part} partB
 * @returns {boolean} True if the two parts have conflicting locations
 */
export function partConflict(partA, partB) {
    if (partA.side !== partB.side) {
        return false;
    }
    // else on same side
    if (Object.getPrototypeOf(partA) === Object.getPrototypeOf(partB)) {
        return true;
    }
    if (extractUnmodifiedLocation(partA.loc) === extractUnmodifiedLocation(partB.loc)) {
        // refuse any other part at location
        if (partA.loc.charAt(0) === "-" || partB.loc.charAt(0) === "-") {
            return true;
        }
        // allow any parts at location (so even if same location would still be OK)
        if (partA.loc.charAt(0) === "+" || partB.loc.charAt(0) === "+") {
            return false;
        }
        // otherwise both are unmodified and conflict
        return true;
    } else {
        return false;
    }
}

export function getChildLocation(parentLoc, child) {
    const parentSide = extractSideLocation(parentLoc);
    let childLoc = child;

    // could potentially not have a side (centered)
    if (parentSide !== null) {
        childLoc = parentSide + " " + childLoc;
    }
    let childSide = getSideValue(parentSide);

    // could get overriden if side explicitely set
    if (extractSideLocation(childLoc)) {
        childSide = getSideValue(extractSideLocation(childLoc));
    }
    return {
        childSide,
        childLoc
    };
}

export function getSideValue(side) {
    if (side === "left" || side === Part.LEFT) {
        return Part.LEFT;
        // could alternatively already be given in numeric terms
    } else {
        return Part.RIGHT;
    }
}

export function getAttachedLocation(partPrototype) {
    const temp = new partPrototype();
    return extractUnmodifiedLocation(temp.loc);
}
