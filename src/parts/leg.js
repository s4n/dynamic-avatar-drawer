import {shadingMedium, ShadingPart} from "../draw/shading_part";
import {Layer} from "../util/canvas";
import {BodyPart} from "./part";
import {
    extractPoint,
    adjust,
    splitCurve,
    continueCurve,
    clone,
    simpleQuadratic,
    clamp,
} from "drawpoint/dist-esm";
import {averagePoint} from "../util/utility";
import {connectEndPoints} from "../draw/draw";

class RightLegShading extends ShadingPart {
    constructor(...data) {
        super({
            loc  : "right leg",
            layer: Layer.MIDRIFT,
        }, ...data);
    }

    fill() {
        return shadingMedium;
    }

    calcDrawPoints(ex) {
        const belly = this.bellyProtrusion * 0.1;
        const hip = adjust(ex.hip, -3 - this.hipWidth * 0.04 + belly * 0.5, -belly);
        const thighOut = adjust(ex.thigh.out, -7 - this.legFullness * 0.2, 0);

        // drawing the kneecap
        const kneeOut = averagePoint(ex.knee.out, ex.knee.in);
        kneeOut.cp1 = continueCurve(hip, thighOut, 1);
        kneeOut.cp2 = {
            x: kneeOut.x + 5,
            y: kneeOut.y
        };

        const sp = splitCurve(0.5, thighOut, kneeOut);
        const kneeCap = sp.left.p2;

        kneeOut.cp1 = {
            x: kneeCap.x,
            y: kneeCap.y - 3
        };


        const kneeBot = {
            x: kneeOut.x * 0.7 + ex.knee.out.x * 0.3,
            y: kneeOut.y - 2
        };
        kneeBot.cp1 = {
            x: kneeOut.x - 2,
            y: kneeOut.y - 2
        };


        const calfOut = adjust(ex.calf.out, -6 - this.legFullness * 0.1,
            0);
        calfOut.cp1 = {
            x: kneeBot.x + 1,
            y: kneeBot.y - 5
        };

        const ankleOut = adjust(ex.ankle.out, -2, 3);
        ankleOut.cp1 = continueCurve(kneeBot, calfOut);

        const outBot = {
            x: ex.ankle.out.x + 20,
            y: ex.ankle.out.y
        };
        const outTop = {
            x: ex.hip.x + 20,
            y: ex.hip.y
        };

        return [
            hip,
            thighOut,
            kneeCap,
            kneeOut,
            kneeBot,
            calfOut,
            ankleOut,
            extractPoint(ex.ankle.out),
            outBot,
            outTop,
            hip
        ];
    }
}


class LeftLegShading extends ShadingPart {
    constructor(...data) {
        super({
            loc  : "left leg",
            layer: Layer.FRONT,
        }, ...data);
    }

    fill() {
        return shadingMedium;
    }

    calcDrawPoints(ex) {

        // const hip = connectEndPoints(ex.thigh.top, ex.hip);
        // const foldDepth = clamp(this.legFem / 55 - this.lowerMuscle / 150 - 0.1, 0, 1);
        // const sp = splitCurve(ex.thigh.top, hip, foldDepth);
        // ex.thigh.fold = sp.left.p2;
        const ankleIn = adjust(ex.ankle.in, 3, 2);
        const calfIn = adjust(ex.calf.in, 10 - this.legFem * 0.15, 0);
        calfIn.cp1.x -= 5;

        // give hint of kneecap
        const kneeBot = {
            x: ex.knee.in.x * 0.7 + ex.knee.out.x * 0.3,
            y: ex.knee.in.y - 3,
        };
        kneeBot.cp1 = continueCurve(ankleIn, calfIn);
        kneeBot.cp2 = {
            x: kneeBot.x + 5,
            y: kneeBot.y - 2
        };

        const kneeTop = adjust(ex.knee.intop, 3, 2);
        kneeTop.cp1 = {
            x: kneeBot.x - 3,
            y: kneeBot.y + 3
        };
        kneeTop.cp2 = {
            x: kneeTop.x - 1,
            y: kneeTop.y - 2
        };

        let sp = splitCurve(0.8, ex.thigh.top, ex.thigh.fold);
        const thighTop = sp.left.p2;
        thighTop.cp1 = simpleQuadratic(kneeTop, thighTop, 0.5, 2);

        const inMid = {
            x: ex.knee.in.x - 2,
            y: ex.knee.in.y
        };
        inMid.cp1 = simpleQuadratic(thighTop,
            inMid,
            0.35,
            -8 - this.legFullness * 0.3 - this.legFem * 0.1);

        const ankleBot = extractPoint(ex.ankle.in);
        ankleBot.cp1 = {
            x: ex.calf.in.x - 3,
            y: ex.calf.in.y
        };

        return [ankleBot, ankleIn, calfIn, kneeBot, kneeTop, thighTop, inMid, ankleBot];

    }
}


class ThighShading extends ShadingPart {
    constructor(...data) {
        super({
            loc    : "+left leg",
            layer  : Layer.FRONT,
            reflect: true,
        }, ...data);
    }

    calcDrawPoints(ex) {
        const fold = clone(ex.thigh.fold);
        const sp = splitCurve(0.85, ex.thigh.in, ex.thigh.top);
        const shiftedBot = sp.left.p2;
        const bot = sp.right.p2;

        shiftedBot.cp1 = clone(fold.cp1);
        fold.cp1.x -= 1;
        fold.cp1.y += 1;
        shiftedBot.cp1.x += 0.5;
        shiftedBot.cp1.y -= 0.5;

        return [bot, fold, shiftedBot, bot];
    }
}


class Leg extends BodyPart {
    constructor(...data) {
        super({
            loc         : "leg",
            layer       : Layer.FRONT,
            childParts  : ["feet"],
            shadingParts: [RightLegShading, LeftLegShading, ThighShading]
        }, ...data);
    }
}


export class LegHuman extends Leg {
    constructor(...data) {
        super(...data);
    }

    calcDrawPoints(ex, ignore, calculate) {
        if (calculate) {
            const scale = this.legLength * 0.01;
            let knee = ex.knee = {};
            knee.out = {
                x  : 12 - clamp(this.legFem * 0.3, 0, 9) + this.hipWidth * 0.04 +
                    this.legFullness * 0.03,
                y  : this.legLength * 0.5,
                cp1: {
                    x: ex.hip.x + this.legFem * 0.1 + this.buttFullness * 0.15 +
                        this.legFullness * 0.1 - this.lowerMuscle * 0.05,
                    y: ex.hip.y - 7 - this.legFem * 0.1 - this.buttFullness * 0.1 -
                        this.legFullness * 0.1,
                },
            };
            knee.out.cp2 = {
                x: knee.out.x + 4 + this.legFem * 0.1 - this.lowerMuscle * 0.05,
                y: knee.out.y + this.legLength * 0.25,
            };

            // different stance with calfs being more prominant
            const bulk = this.lowerMuscle - 10;
            const stanceMusculature = this.legFem < 25 && bulk > 0;
            if (stanceMusculature) {
                knee.out.cp1.x += bulk * 0.05;
                knee.out.cp1.y -= bulk * 0.05;
                knee.out.cp2.x += bulk * 0.2;
                knee.out.cp2.y += bulk * 0.5;
                ex.hip.cp2.x += bulk * 0.03;
            }

            let sp = splitCurve(0.55, ex.hip, knee.out);

            let thigh = ex.thigh = ex.thigh || {};
            thigh.out = sp.left.p2;
            // reassign other draw points
            knee.out = sp.right.p2;

            ex.ankle = ex.ankle || {};
            ex.ankle.out = {
                x: knee.out.x - 0.5 - this.legFullness * 0.05,
                y: 8,
            };

            let calf = ex.calf = ex.calf || {};
            calf.out = {
                x  : knee.out.x + (clamp(this.legFem * 0.1, 0, 3) +
                    clamp(this.legFullness * 0.005, 0, 3)) * scale,
                y  : knee.out.y - this.legLength * 0.20 + this.lowerMuscle * 0.1,
                cp1: {
                    x: knee.out.x,
                    y: knee.out.y - this.legLength * 0.1 +
                        clamp(this.legFem * 0.05, 0, 6),
                },
            };
            calf.out.cp2 = {
                x: calf.out.x,
                y: calf.out.y + this.legLength * 0.10,
            };

            ex.ankle.out.cp1 = {
                x: calf.out.x,
                y: calf.out.y - this.legLength * 0.12,
            };
            ex.ankle.out.cp2 = {
                x: ex.ankle.out.x - 1,
                y: ex.ankle.out.y + 5
            };


            // feet ------------

            ex.ankle.in = {
                x: ex.ankle.out.x - 5 + this.legFem * 0.02,
                y: ex.ankle.out.y + 0.5,
            };

            calf.in = {
                x: calf.out.x - 9 + this.legFem * 0.015 - this.legFullness * 0.05,
                y: calf.out.y - this.legFem * 0.1,
            };
            const stanceMasculinity = 15 - this.legFem;
            if (stanceMasculinity > 0) {
                calf.in.x -= stanceMasculinity * 0.02;
            } else {
                calf.in.x += this.legFem * 0.02;
            }
            calf.in.cp1 = {
                x: ex.ankle.in.x + 0.5,
                y: ex.ankle.in.y + (1.3 - this.legFem * 0.03) * this.legLength * 0.1
            };
            calf.in.cp2 = {
                x: calf.in.x + this.legFem * 0.02,
                y: calf.in.y + (this.legFem * 0.01 - 1) * this.legLength * 0.1,
            };
            if (stanceMasculinity > 0) {
                calf.in.cp2.x += stanceMasculinity * 0.01;
            }


            knee.in = {
                x: knee.out.x - 8 - this.legFullness * 0.05,
                y: knee.out.y,
            };
            knee.in.cp1 = continueCurve(ex.ankle.in, calf.in, 0.7);


            knee.intop = {
                x: knee.in.x + 0.2 - this.legFem * 0.01,
                y: knee.in.y + 3,
            };

            if (stanceMusculature) {
                knee.in.x -= bulk * 0.04;
                knee.intop.x -= bulk * 0.04;
            }

            knee.in.cp2 = {
                x: knee.in.x,
                y: knee.in.y - this.legLength * 0.09
            };

            thigh.top = {
                x  : 1.6,
                y  : ex.hip.y - this.legLength * 0.15,
                cp1: {
                    x: knee.in.x,
                    y: knee.in.y + this.legLength * 0.07
                },
            };
            thigh.top.cp2 = {
                x: 2 - this.buttFullness * 0.03 - this.legFem * 0.04,
                y: thigh.top.y - this.legLength * 0.12
            };

            // different stance with calfs being more prominant
            if (stanceMusculature) {
                // bulk up calf
                knee.in.cp2.x -= bulk * 0.01;
                knee.in.cp2.y += bulk * 0.05;
                knee.in.cp1.x -= bulk * 0.1;
                calf.in.x -= bulk * 0.1;
                calf.in.y += bulk * 0.1;
                calf.in.cp2.x -= bulk * 0.1;
                calf.out.x += bulk * 0.05;

                calf.out.cp2.x += bulk * 0.05;
                calf.out.cp1.x += bulk * 0.02;
                calf.out.cp1.y += bulk * 0.1;
                // tighten up inner thigh
                thigh.top.cp2.x -= bulk * 0.08;
                thigh.top.cp2.y -= bulk * 0.08;
                thigh.top.cp1.y += bulk * 0.2;
            }


            // split into inner thigh
            sp = splitCurve(0.55, knee.in, thigh.top);

            thigh.in = sp.left.p2;

            // reassign other draw points
            thigh.top = sp.right.p2;

            const hip = connectEndPoints(thigh.top, ex.hip);
            const foldDepth = clamp(this.legFem / 55 - this.lowerMuscle / 150 - 0.1, 0, 1);
            sp = splitCurve(foldDepth, thigh.top, hip);
            thigh.fold = sp.left.p2;
        }

        return [
            extractPoint(ex.hip),
            ex.thigh.out,
            ex.knee.out,
            ex.calf.out,
            ex.ankle.out,
            {child: "feet"},
            ex.ankle.in,
            ex.calf.in,
            ex.knee.in,
            ex.knee.intop,
            ex.thigh.in,
            ex.thigh.top,
        ];
    }
}
