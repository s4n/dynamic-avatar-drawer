import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Part} from "../parts/part";
import {Layer} from "../util/canvas";
import {
    simpleQuadratic,
    drawPoints,
    extractPoint,
    splitCurve,
	interpolateCurve,
	adjust,
    clone,
} from "drawpoint/dist-esm";

import {
	getLimbPointsNegative,
	getLimbPointsBellowPoint,
	getLimbPointsAbovePoint,
	findBetween,
} from "../util/auxiliary";

import {Sock,SockPart} from "./socks";
import {calcSuperPantsTop} from "./super_pants";

function calcSuperSocks(ex){
	
	let  addPointsMid=[];
	if(typeof ex.quads !== "undefined"){
		addPointsMid[0] =  clone(ex.quads.top);
		addPointsMid[0].x = ex.thigh.out.x;
		addPointsMid[1] = clone(ex.quads.out);
	}
	
	let  outerPoints = getLimbPointsNegative(ex.hip,ex.ankle.out,this.legCoverage,ex.hip,ex.thigh.out, ...addPointsMid, ex.knee.out,ex.calf.out,ex.ankle.out);
	
	if(!this.lockGroin&&this.legCoverage<0){ //can include waist 
		let torsoPoints = getLimbPointsNegative(ex.armpit,ex.hip,(1+this.legCoverage),ex.armpit,ex.waist,ex.hip);
		torsoPoints.pop();
		outerPoints = torsoPoints.concat(outerPoints);
	}
	
	let  innerPoints = getLimbPointsBellowPoint(outerPoints[0],true,ex.groin,ex.thigh.top,ex.thigh.in,ex.knee.intop,ex.knee.in,ex.calf.in,ex.ankle.in);
		
	outerPoints[0] = extractPoint( outerPoints[0]); 

	 //TOP
	 //socks top curve
	 if(outerPoints[0].y-6<=ex.thigh.top.y){
		outerPoints[0].cp1 = {
			x:findBetween(innerPoints[innerPoints.length-1].x,outerPoints[0].x,0.5),
			y:outerPoints[0].y-6
		};
	//should be tights but manually overridden to stay socks (stockings)
	}else if(this.lockGroin){
		innerPoints.splice(innerPoints.length-1, 1);
		let temp = interpolateCurve( ex.hip, ex.thigh.out, {x:null,y:ex.thigh.top.y});
		outerPoints[0] = extractPoint(temp[0]);
		outerPoints[1] = extractPoint(outerPoints[1]);//lasso loop above outerPoints[0]; 
		outerPoints[0].cp1 = {
			x:findBetween(innerPoints[innerPoints.length-1].x,outerPoints[0].x,0.5),
			y:outerPoints[0].y-6
		};		
	//tights - top & top curve
	}else{
		innerPoints[innerPoints.length] = adjust(ex.groin,-0.2,0);
		
		const waistCurve = outerPoints[0].y - ex.hip.y;
		let  top = {
			x: -0.2,  
			y: ex.pelvis.y + waistCurve * 1.2
		}; //formula from original pants 
		
		outerPoints[0].cp2 = void 0;
		outerPoints[0].cp1 = {
			x: findBetween(outerPoints[0].x,top.x,0.5),
			y: top.y
		};
		
		outerPoints.unshift(top);
		
		//arch between thighs 
		innerPoints[innerPoints.length-1].cp1 = {
			x: innerPoints[innerPoints.length-1].x * 0.5 + ex.thigh.top.x * 0.5,
			y: innerPoints[innerPoints.length-1].y
		};
		
		innerPoints[innerPoints.length] = top;
	}
	
	return {
		outerPoints,
		innerPoints,
  };
}

export class Garter extends SockPart {
    constructor(...data) {
        super({
            aboveSameLayerParts: ["feet"],
			aboveParts: ["parts feet","parts leg","clothingParts groin"],
			reflect:true,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		const {outerPoints, innerPoints} = calcSuperSocks.call(this,ex);
		//Clothes.simpleStrokeFill(ctx, ex, this);
		ctx.fillStyle = this.highlight;
		
		//TOP
	
		//based on code from from calcSuperPants
		let outTop;
		let outBots;
		if(this.waistCoverage>0){
			let sp = splitCurve((1-this.waistCoverage),ex.waist,ex.hip);	
			outTop = extractPoint(sp.left.p2);
			//let  hip = temp.right.p2;
			let temp = {x:0,y:outTop.y-this.beltWidth};
			outBots = getLimbPointsAbovePoint(temp,false,outTop,ex.hip,ex.thigh.out);
			//let  thighOut = ex.thigh.out;
		}else{
			let sp = splitCurve(Math.abs(this.waistCoverage),ex.hip,ex.thigh.out);	
			outTop = extractPoint(sp.left.p2);
			//let  hip;
			let temp = {x:0,y:outTop.y-this.beltWidth};
			outBots = getLimbPointsAbovePoint(temp,false,outTop,ex.thigh.out);
			//let  thighOut = temp.right.p2;
		}
		
		
		//let  waistCurve = outBots[outBots.length-1].y - ex.hip.y;
		let  inTop = {
			x: -0.1,  
			y: outTop.y-6,
			//y: ex.pelvis.y + waistCurve * 1.2//1.2
		};
		outTop.cp1 = {
			x: outTop.x * 0.5 + inTop.x * 0.5,
			y: inTop.y
		};
		
		//CONNECTION TO SOCKS
		let  sockIn =  innerPoints[innerPoints.length-1];
		let  sockOut = extractPoint(outerPoints[0]);
		sockIn.y-=6;		
		sockOut.y-=6;
		
		
		let  connectOut = {
			y:sockOut.y,
			x:(((sockIn.x-sockOut.x)*0.4+sockOut.x)  )+3
		};
		connectOut.cp1 = {
			y: ((outBots[outBots.length-1].y-connectOut.y)*0.9)+connectOut.y,
			x: ((outBots[outBots.length-1].x-connectOut.x)*(-0.1))+connectOut.x
		};
		let  connectIn = {
			y: sockIn.y,
			x: connectOut.x-3,
		};
		let  inBot = {
			x: ex.pelvis.x-0.1,
			y: inTop.y-(outTop.y-outBots[outBots.length-1].y)
		};
		inBot.cp1 = {
			y: ((connectIn.y-inTop.y)*0.2)+inTop.y,
			x: ((connectIn.x-inTop.x)*(0.9))+inTop.x
		};
		
		ctx.beginPath();
        drawPoints(
            ctx,  
			inTop,
			outTop,
			...outBots,
			connectOut,
			connectIn,
			inBot
		);
        ctx.fill();
      //  ctx.stroke();
    }
}		
		
		
		
export class SuperSockBandPart extends SockPart {
    constructor(...data) {
        super({
            aboveSameLayerParts: ["feet"],
			aboveParts: ["parts feet","parts leg","clothingParts groin"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		if(this.bandWidth<0.1)return;
		
		const {outerPoints, innerPoints} = calcSuperSocks.call(this,ex);
		//Clothes.simpleStrokeFill(ctx, ex, this);
		ctx.fillStyle = this.highlight;
		
		let  topIn = innerPoints[innerPoints.length-1];
		let  topOut = extractPoint(outerPoints[0]);
		
		let tempPoint = clone(topIn);
		tempPoint.y = tempPoint.y-this.bandWidth;
		
		innerPoints.reverse(); //they are already in order for drawPoints (lowest to highest) but 
		let  botIns  = getLimbPointsAbovePoint(tempPoint,true,...innerPoints);		
		let  botOuts = getLimbPointsAbovePoint(tempPoint,false,...outerPoints);
		
		//top curve
		topOut.cp1 = {
			x:findBetween(topIn.x,topOut.x,0.5),
			y:topOut.y-6
		};
		
		//bottom curve
		botIns[0] = extractPoint(botIns[0]);
		botIns[0].cp1 = {
			x:findBetween(botOuts[botOuts.length-1].x,botIns[0].x,0.5),
			y:botIns[0].y-6
		};
				
		ctx.beginPath();
        drawPoints(
            ctx,  
			topOut, 
			...botOuts,
			...botIns, 
			topIn,
			topOut
		);
        ctx.fill();
        //ctx.stroke();
    }
}
		
		
export class SuperSockPart extends SockPart {
    constructor(...data) {
        super({
            aboveSameLayerParts: ["feet"],
			aboveParts: ["parts feet","parts leg","clothingParts groin"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		const {outerPoints, innerPoints} = calcSuperSocks.call(this,ex);
		
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		ctx.beginPath();
        drawPoints(
            ctx,  
			...outerPoints, 
			ex.ankle.outbot, ex.toe.out, ex.toe.in, ex.ankle.inbot,
			...innerPoints, 
			outerPoints[0]
		);
        ctx.fill();
        ctx.stroke();
		
    }
}


export class PantyhosePart extends SockPart {
    constructor(...data) {
        super({
            aboveSameLayerParts: ["feet"],
			aboveParts: ["parts feet","parts leg","clothingParts groin"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		const {top, out, hip, thighOut} = calcSuperPantsTop.call(this,ex);
		Clothes.simpleStrokeFill(ctx, ex, this);
	
		const addPointsMid = []; //ie should be muscles included?
		if(typeof ex.quads !== "undefined"){
			addPointsMid[0] =  clone(ex.quads.top);
			addPointsMid[0].x =  ex.thigh.out.x;
			addPointsMid[1] = clone(ex.quads.out);
		}
		
		let thigh = ex.thigh.top;
		let groin = ex.groin;
		
		if(this.open>0.02){
			let temp = (top.y-groin.y)*0.7*this.open;
			groin = adjust(ex.groin,0,temp)
					
			groin.cp2 = adjust(ex.groin,22*this.open,22*this.open)
			groin.cp1 = adjust(ex.groin,12*this.open,-14*this.open)
					
			temp = da.splitCurve(1-(0.9*this.open),ex.thigh.in,ex.thigh.top);
			thigh = temp.left.p2;
		};
				
		ctx.beginPath();
        drawPoints(ctx,  
			top,out,hip,thighOut,
			...addPointsMid,
			ex.knee.out,ex.calf.out,ex.ankle.out,
			ex.ankle.outbot, ex.toe.out, ex.toe.in, ex.ankle.inbot,
			ex.ankle.in,ex.calf.in,ex.knee.in,ex.knee.intop,ex.thigh.in,
			thigh,
			groin, top
		);
        ctx.fill();
        ctx.stroke();
		
    }
}



export class SuperSocks extends Sock {
    constructor(...data) {
        super({
			legCoverage: 0.5,
			thickness: 0.5,
			lockGroin: false,
		},...data);
    }

    get partPrototypes() {
        return [
            {
                side: Part.LEFT,
                Part: SuperSockPart,
            },
            {
                side: Part.RIGHT,
                Part: SuperSockPart,
            },
        ];
    }
}

export class Stockings extends Sock {
    constructor(...data) {
        super({
			legCoverage: 0.2,
			thickness: 0.3,
			bandWidth  : 7,
			highlight: "hsla(0, 0%, 11%, 1)",
			lockGroin: true,
		},...data);
    }

	fill() {
        return "hsla(0, 0%, 28%, 1)";
    }
	
    get partPrototypes() {
        return [
            {
                side: Part.LEFT,
                Part: SuperSockBandPart,
            },
            {
                side: Part.RIGHT,
                Part: SuperSockBandPart,
            },
			
			{
                side: Part.LEFT,
                Part: SuperSockPart,
            },
            {
                side: Part.RIGHT,
                Part: SuperSockPart,
            },
			
        ];
    }
}

export class StockingsGarter extends Sock {
    constructor(...data){
        super({
			legCoverage: 0.2,
			thickness: 0.3,
			bandWidth  : 9,
			beltWidth : 10,
			highlight: "hsla(0, 0%, 11%, 1)",
			waistCoverage: 0.5,
			lockGroin: true,
		},...data);
    }

	fill(){
        return "hsla(0, 0%, 28%, 1)";
    }
	
    get partPrototypes(){
        return [
            {
				side: Part.RIGHT,
				Part: Garter
			}, 
			{
                side: Part.LEFT,
                Part: SuperSockBandPart,
            },
            {
                side: Part.RIGHT,
                Part: SuperSockBandPart,
            },
			
			{
                side: Part.LEFT,
                Part: SuperSockPart,
            },
            {
                side: Part.RIGHT,
                Part: SuperSockPart,
            },
			
        ];
    }
}



export class Pantyhose extends Sock {
    constructor(...data) {
        super({
			waistCoverage : 0.45,
			thickness : 0.3,
			open: 0,
		},...data);
    }
	
	fill(){
        return "hsla(0,20%,30%,0.5)";
    }
	
	stroke(){
        return "hsla(0.0,5%,30%,1)";
    }
	
    get partPrototypes() {
        return [
            {
                side: Part.LEFT,
                Part: PantyhosePart,
            },
            {
                side: Part.RIGHT,
                Part: PantyhosePart,
            },
        ];
    }
}