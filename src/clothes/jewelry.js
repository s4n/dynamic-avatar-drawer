import {Clothes, ClothingPart, Clothing, processClothingPartLocation} from "./clothing";
import {Layer} from "../util/canvas";
import {locateRelativeToDrawpoint, Location} from "..";
import {
    drawPoints,
    rad,
    point,
    norm,
    diff
} from "drawpoint/dist-esm";


/**
 * ClothingPart drawn classes/components
 */
export class RingBotPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.BELOW_HAIR,
            loc       : `${Location.EAR}`,
            reflect   : false,
            belowParts: [`faceParts ${Location.EAR}`],
        }, {
            radius: 2,
        }, ...data);
    }


    renderClothingPoints(ex, ctx) {
        const center = locateRelativeToDrawpoint(ex, this.relativeLocation);
        Clothes.simpleStrokeFill(ctx, ex, this);
        ctx.beginPath();
        ctx.arc(center.x, center.y, this.radius, 0, 2 * Math.PI);
        ctx.stroke();
    }
}

export class RingTopPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.BELOW_HAIR,
            loc       : `${Location.EAR}`,
            reflect   : false,
            aboveParts: [`faceParts ${Location.EAR}`],
        }, ...data);
    }


    renderClothingPoints(ex, ctx) {
        const center = locateRelativeToDrawpoint(ex, this.relativeLocation);
        Clothes.simpleStrokeFill(ctx, ex, this);
        ctx.beginPath();
        const rot = rad(this.rotation);
        ctx.arc(center.x, center.y, this.radius, rot, Math.PI + rot);
        ctx.stroke();
    }
}

export class StudPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.BELOW_HAIR,
            loc       : `${Location.NOSE}`,
            reflect   : false,
            aboveParts: [`faceParts ${Location.NOSE}`],
        }, {
            radius: 0.3,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        const center = locateRelativeToDrawpoint(ex, this.relativeLocation);
        Clothes.simpleStrokeFill(ctx, ex, this);
        ctx.beginPath();
        const dps = da.drawCircle(center, this.radius);
        // ctx.arc(center.x, center.y, this.radius, 0, Math.PI*2);
        drawPoints(ctx, ...dps);
        ctx.fill();
    }
}

export class ChainPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.HAIR,
            loc       : `+${Location.NOSE}`,
            reflect   : false,
        }, {
            thickness: 0.7,
            slack    : 1,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        const start = locateRelativeToDrawpoint(ex, this.relativeLocation);
        const end = locateRelativeToDrawpoint(ex, this.endRelativeLocation);
        Clothes.simpleStrokeFill(ctx, ex, this);
        // middle point is somewhere in between
        end.cp1 = point((start.x + end.x) / 2,
            (start.y + end.y) / 2 - this.slack * norm(diff(start, end)));
        ctx.beginPath();
        ctx.setLineDash([1,1]);
        drawPoints(ctx, start, end);
        ctx.stroke();
        ctx.setLineDash([]);
    }
}


/**
 * Base Clothing classes
 */
export class Jewelry extends Clothing {
    constructor(...data) {
        super({
            clothingLayer: Clothes.Layer.INNER,
            stroke       : "rgb(255,223,0)",
            fill         : "rgb(255,223,0)",
        }, ...data);
    }
}

export class Piercing extends Jewelry {
    constructor(...data) {
        super({
            rotation     : 0,
        }, ...data);
        // should also apply location and side properties to part prototypes
        // more convenient than requiring users to apply the same properties to both the jewelry
        // and all the parts
        this.parts.forEach((part) => {
            part.loc = this.loc;
            processClothingPartLocation(this.side, part);
            // TODO consider also applying location on aboveParts and belowParts
        });
    }
}

/**
 * Concrete Clothing classes
 */
export class RingPiercing extends Piercing {
    constructor(...data) {
        super({
            relativeLocation: {
                drawpoint: "ear.mid",
                dx       : 0,
                dy       : 0
            },
            loc             : `${Location.EAR}`,
            requiredParts   : "faceParts",
        }, ...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: RingBotPart,
            },
            {
                side: null,
                Part: RingTopPart,
            },
        ];
    }
}

export class StudPiercing extends Piercing {
    constructor(...data) {
        super({
            relativeLocation: {
                drawpoint: "nose.out",
                dx       : 0,
                dy       : 0
            },
            loc             : `${Location.NOSE}`,
            requiredParts   : "faceParts",
        }, ...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: StudPart,
            },
        ];
    }
}

export class ChainJewelry extends Piercing {
    constructor(...data) {
        super({
            relativeLocation   : {
                drawpoint: "nose.out",
                dx       : 0,
                dy       : 0
            },
            endRelativeLocation: {
                drawpoint: "ear.mid",
                dx       : 0,
                dy       : 0
            },
            loc                : `${Location.NOSE}`,
            requiredParts      : "faceParts",
        }, ...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: ChainPart,
            },
        ];
    }
}