import {Clothes, ClothingPart, Clothing} from "./clothing";
import {shine} from "../draw/shading_part";
import {Part} from "../parts/part";
import {Layer} from "../util/canvas";
import {setStrokeAndFill} from "../util/draw";
import {ShoeSidePart} from "./shoes";
import {
    reverseDrawPoint,
    splitCurve,
    simpleQuadratic,
    drawPoints,
    extractPoint,
    adjust,
    continueCurve,
    scale, scalePoints, rotatePoints, breakPoint
} from "drawpoint/dist-esm";

export class HeelPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.FRONT,
            loc       : "feet",
            aboveParts: ["parts feet", "parts leg"],
            belowParts: ["shadingParts feet"],
            // aboveSameLayerParts: ["feet"],
        }, ...data);
    }
}


export class HeelBaseShine extends HeelPart {
    constructor(...data) {
        super({
            layer: Layer.ARMS,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        // fan shaped shine
        const {bot, outBot, out} = calcHeels.call(this, ex);
        bot.y += 3;
        bot.x += 1;

        const left = adjust(outBot, -1, 0.5);
        const top = adjust(out, -0.5, -4.8);

        left.cp1 = simpleQuadratic(bot, left, 0.7, -1);
        top.cp1 = simpleQuadratic(left, top, 0.5, -0.5);
        bot.cp1 = simpleQuadratic(top, bot, 0.3, 1);

        ctx.fillStyle = shine;
        ctx.beginPath();
        drawPoints(ctx, bot, left, top, bot);
        ctx.fill();
    }
}


export class HeelBasePart extends HeelPart {
    constructor(...data) {
        super(...data);
    }

    renderClothingPoints(ex, ctx) {

        const toe = ex.toe;

        toe.toebox = {
            x: toe.center.x,
            y: toe.center.y
        };

        const {out, outBot, bot, inBot, inTop, tongue} = calcHeels.call(this, ex);

        Clothes.simpleStrokeFill(ctx, ex, this);
        ctx.beginPath();
        drawPoints(ctx, out, outBot, bot, inBot, inTop, tongue, out);
        ctx.fill();

        // back of the heels
        const backTop = extractPoint(ex.ankle.in);
        let sp = splitCurve(0.6, ex.toe.in, ex.ankle.inbot);
        const backBot = sp.left.p2;
        backBot.cp1 = simpleQuadratic(backTop, backBot, 0.2, -3.3);
        backTop.cp1 = simpleQuadratic(backBot, backTop, 0.7, 1.4);

        ctx.beginPath();
        drawPoints(ctx, backTop, backBot, backTop);
        ctx.fill();

    }
}

export class CoveredHeelBasePart extends HeelPart {
    constructor(...data) {
        super(...data);
    }

    renderClothingPoints(ex, ctx) {

        const {out, outBot, bot, inBot, inTop} = calcHeels.call(this, ex);

        // coverage part
        const topLeft = adjust(extractPoint(ex.ankle.in), 1.1, -0.7);
        const topRight = adjust(extractPoint(ex.ankle.out), -1.1, -0.7);
        out.cp1 = simpleQuadratic(topRight, out, 0.5, -1);
        out.cp2 = null;
        topLeft.cp1 = simpleQuadratic(inTop, topLeft, 0.5, -1);

        Clothes.simpleStrokeFill(ctx, ex, this);
        ctx.beginPath();
        drawPoints(ctx, out, outBot, bot, inBot, inTop, topLeft, topRight, out);
        ctx.fill();

    }
}


export class HeelStrapPart extends HeelPart {
    constructor(...data) {
        super({strapWidth: 1.5}, ...data);
    }

    renderClothingPoints(ex, ctx) {

        let sp = splitCurve(1.06 - this.strapWidth * 0.03, ex.calf.out, ex.ankle.out);

        const topRight = sp.right.p1;
        const right = extractPoint(ex.ankle.out);
        const left = extractPoint(ex.ankle.in);
        right.cp1 = sp.right.cp1;
        right.cp2 = sp.right.cp2;

        const deflection = 3;
        left.cp1 = simpleQuadratic(right, left, 0.5, deflection);

        sp = splitCurve(this.strapWidth * 0.035, ex.ankle.in, ex.calf.in);
        const topLeft = extractPoint(sp.left.p2);
        topRight.cp1 = simpleQuadratic(topLeft, topRight, 0.5, -deflection);

        setStrokeAndFill(ctx,
            {
                fill  : this.fill,
                stroke: this.fill
            },
            ex);
        ctx.beginPath();
        drawPoints(ctx, topRight, right, left, topLeft, topRight);
        ctx.fill();


    }
}


export function calcHeels(ex) {
    const toe = ex.toe;
    // draw from outside in
    let sp = splitCurve(0.7, ex.ankle.outbot, toe.out);
    const out = sp.left.p2;

    const outBot = {
        x: toe.out.x + 2,
        y: toe.in.y + 2,
    };

    const bot = {
        x: toe.out.x * 0.45 + toe.in.x * 0.55,
        y: toe.in.y - this.heelPointiness
    };

    const inBot = {
        x: toe.in.x - 2.5,
        y: toe.in.y + 2
    };

    sp = splitCurve(0.6, toe.in, ex.ankle.inbot);
    const inTop = sp.left.p2;

    const tongue = {
        x: bot.x,
        y: toe.center.y - 2.3 + this.toeCoverage * 1.3
    };

    bot.cp1 = simpleQuadratic(outBot, bot, 0.6, 2);
    inBot.cp1 = simpleQuadratic(bot, inBot, 0.6, 2);
    tongue.cp1 = scale(inTop, 0.5, inBot);
    tongue.cp2 = {
        x: tongue.x - 4,
        y: tongue.y
    };
    out.cp1 = continueCurve(inTop, tongue, 1);
    out.cp2 = scale(out, 0.5, outBot);

    outBot.cp1 = simpleQuadratic(out, outBot, 0.8, 0.6);
    inTop.cp1 = simpleQuadratic(inBot, inTop, 0.1, 1);

    return {
        out,
        outBot,
        bot,
        inBot,
        inTop,
        tongue
    };
}


export class HeelSideBaseClosedPart extends ShoeSidePart {
    constructor(...data) {
        super(...data, {
            toeCoverage: 0.5,
        });
    }

    renderShoeSidePart(ex, ctx, mods) {
        const {
            vamp,
            toeTip,
            soleBot,
            heelBack,
            counterTip,
        } = calcHeelBaseSide.call(this, ex, mods);

        ctx.beginPath();
        setStrokeAndFill(ctx,
            {
                stroke: this.fill,
                fill  : this.fill
            },
            ex);
        drawPoints(ctx,
            vamp,
            toeTip,
            soleBot,
            heelBack,
            counterTip,
            vamp);
        ctx.fill();
    }
}

export class CoveredHeelSideBasePart extends ShoeSidePart {
    constructor(...data) {
        super(...data, {toeCoverage: 0.2, soleWidth: 3.0, tongueSplitRatio: 0.75});
    }

    renderShoeSidePart(ex, ctx, mods) {

        const {
            vamp,
            toeTip,
            soleBot,
            heelBack,
            heelBackTop,
            tongueStart,
            tongueSide,
            tongue
        } = calcCoveredHeelBaseSide.call(this, ex, mods);

        ctx.beginPath();
        setStrokeAndFill(ctx,
            {
                stroke: this.fill,
                fill  : this.fill
            },
            ex);
        drawPoints(ctx,
            vamp,
            toeTip,
            soleBot,
            heelBack,
            heelBackTop,
            tongueStart,
            tongueSide,
            tongue,
            vamp);
        ctx.fill();
    }
}

export function calcCoveredHeelBaseSide(ex, mods) {
    const {
        vamp,
        toeTip,
        soleBot,
        heelBack,
        counterTip,
    } = calcHeelBaseSide.call(this, ex, mods);
    toeTip.cp1.y += 1;

    // does not actually have a counter
    const sp = splitCurve(this.tongueSplitRatio, soleBot, heelBack);
    let tongueStart = sp.right.p1;
    const rev = reverseDrawPoint(sp.right.p1, sp.right.p2);
    tongueStart.cp1 = rev.cp1;
    tongueStart.cp2 = rev.cp2;

    tongueStart = adjust(tongueStart, 0, this.soleWidth);
    const heelBackTop = adjust(extractPoint(heelBack), 0, this.soleWidth);
    heelBackTop.cp1 = simpleQuadratic(heelBack, heelBackTop, 0.5, -1);

    const tongueSide = {x: counterTip.x - 16, y: counterTip.y};
    tongueSide.cp1 = simpleQuadratic(tongueStart, tongueSide, 0.5, 5);
    const tongue = {x: tongueSide.x - 5, y: tongueSide.y};
    vamp.cp1 = {x: tongue.x, y: tongue.y - 15};
    vamp.cp2 = {x: vamp.x + 6, y: vamp.y};

    return {
        vamp,
        toeTip,
        soleBot,
        heelBack,
        heelBackTop,
        tongueStart,
        tongueSide,
        tongue,
        counterTip,
    };
}

export class StilettoPart extends ShoeSidePart {
    constructor(...data) {
        super(...data, {
            // how much along the path (0 to 1) from sole bottom to heelBack to start extending down
            separationAlongArch: 0.83,
            heelTipWidth       : 3.5,
            // which way and how much the back of the heel bends (can be negative)
            backHeelDeflection : 2,
            // use null to inherit the base's fill
            stilettoFill       : "black",
        });
    }

    renderShoeSidePart(ex, ctx, mods) {

        const {archTip, heelTip, heelTipBack, stilettoHeelBack} = calcStiletto.call(this, ex, mods);

        const fill = this.stilettoFill || this.fill;

        ctx.beginPath();
        setStrokeAndFill(ctx,
            {
                stroke: fill,
                fill  : fill
            },
            ex);
        drawPoints(ctx,
            archTip, heelTip, heelTipBack, stilettoHeelBack, archTip);
        ctx.fill();
    }
}

export class PlatformSidePart extends ShoeSidePart {
    constructor(...data) {
        super(...data, {
            // how high the platform is in mm
            platformHeight: 0,
            platformFill  : "black",
        });
    }

    renderShoeSidePart(ex, ctx, mods) {
        if (this.platformHeight <= 0) {
            return;
        }

        const {topLeft, left, bot, right} = calcHeelPlatformSide.call(this, ex, mods);

        const fill = this.platformFill || this.fill;

        ctx.beginPath();
        setStrokeAndFill(ctx,
            {
                stroke: fill,
                fill  : fill
            },
            ex);
        drawPoints(ctx,
            topLeft, left, bot, right, topLeft);
        ctx.fill();
    }
}

export class HeelSideSimpleStrapPart extends ShoeSidePart {
    constructor(...data) {
        super(...data);
    }

    renderShoeSidePart(ex, ctx, mods) {

        const {counterTip} = calcHeelBaseSide.call(this, ex, mods);
        const strapRadius = 10;
        const center = adjust(counterTip, -strapRadius, 0);

        ctx.beginPath();
        setStrokeAndFill(ctx,
            {
                stroke: this.fill,
                fill  : this.fill
            },
            ex);
        ctx.lineWidth = this.strapWidth * 1.33;
        ctx.ellipse(center.x - 1,
            center.y - 2,
            strapRadius,
            3.5,
            -0.95 * Math.PI,
            0,
            2 * Math.PI);
        ctx.stroke();
    }
}

export class HeelSideWideStrapPart extends ShoeSidePart {
    constructor(...data) {
        super({strapWidth: 5.0}, ...data);
    }

    renderShoeSidePart(ex, ctx, mods) {

        const strap = calcWideStrapSide.call(this, ex, mods);

        ctx.beginPath();
        setStrokeAndFill(ctx,
            {
                stroke: this.fill,
                fill  : this.fill
            },
            ex);

        drawPoints(ctx,
            strap.left,
            strap.topLeft,
            strap.topRight,
            strap.right,
            strap.left);
        ctx.fill();
    }
}

export function calcWideStrapSide(ex, mods) {
    const {tongue: left, counterTip: right} = calcCoveredHeelBaseSide.call(this, ex, mods);
    // rename to make spacial sense

    const topLeft = {x: left.x, y: left.y + this.strapWidth};
    const topRight = {x: right.x - 0.5, y: right.y + this.strapWidth};
    topRight.cp1 = simpleQuadratic(topLeft, topRight, 0.5, -2.5);
    left.cp1 = simpleQuadratic(right, left, 0.5, 2.5);

    right.cp1 = simpleQuadratic(topRight, right, 0.5, -0.5);
    right.cp2 = null;
    return {left, topLeft, topRight, right};
}

const bottomY = 3;

export function calcHeelBaseSide(ignore, mods) {
    const toeTip = {
        x: 5,
        y: 12 + this.platformHeight,
    };
    const soleBot = {
        x: toeTip.x + 14 - this.shoeHeight * 0.5,
        y: toeTip.y - 9
    };
    soleBot.cp1 = {
        x: toeTip.x,
        y: toeTip.y - 3
    };
    soleBot.cp2 = {
        x: soleBot.x - 8,
        y: soleBot.y
    };

    // top of the arch near when the heel extends down closer to the toes
    const archTip = {
        x: toeTip.x + 52 + mods.feetLength * 0.5 - this.shoeHeight * 5,
        y: toeTip.y + this.shoeHeight * 5
    };

    const heelBack = {
        x: archTip.x + 10,
        y: archTip.y
    };
    heelBack.cp1 = {
        x: soleBot.x + 28,
        y: soleBot.y
    };
    heelBack.cp2 = {
        x: archTip.x - 20,
        y: archTip.y - 10
    };

    const counterTip = {
        x: archTip.x + 4,
        y: heelBack.y + 19
    };
    counterTip.cp1 = {
        x: heelBack.x + 3,
        y: heelBack.y + 10
    };
    counterTip.cp2 = {
        x: counterTip.x + 3,
        y: counterTip.y - 5
    };

    const vamp = {
        x: toeTip.x + 10.5 + this.toeCoverage * 1.7 - this.shoeHeight * 0.5,
        y: toeTip.y + 4 + this.toeCoverage * 1.3
    };
    toeTip.cp1 = simpleQuadratic(vamp, toeTip, 0.7, -1.5);
    vamp.cp1 = {
        x: counterTip.x - 19,
        y: counterTip.y - 5
    };
    vamp.cp2 = {
        x: vamp.x + 18,
        y: vamp.y - 10
    };

    return {
        vamp,
        toeTip,
        soleBot,
        heelBack,
        counterTip,
    };
}

export function calcHeelPlatformSide(ex, mods) {
    const {
        toeTip,
        soleBot,
        heelBack
    } = calcHeelBaseSide.call(this, ex, mods);

    const topLeft = extractPoint(toeTip);
    const left = adjust(topLeft, this.platformHeight * 0.2, -this.platformHeight);
    left.cp1 = simpleQuadratic(topLeft, left, 0.3, -0.4);

    const bot = adjust(soleBot, 0, -this.platformHeight);
    bot.cp1.x += this.platformHeight * 0.2;
    // bot.cp1 = null;

    const sp = splitCurve(0.5, soleBot, heelBack);
    const right = sp.left.p2;
    right.cp1.y -= this.platformHeight;
    right.cp2.y -= this.platformHeight * 0.5;

    return {topLeft, left, bot, right};
}

export function calcStiletto(ex, mods) {
    const {
        soleBot,
        heelBack,
    } = calcHeelBaseSide.call(this, ex, mods);

    const sp = splitCurve(this.separationAlongArch, soleBot, heelBack);

    let archTip = extractPoint(sp.left.p2);

    // closer to the back
    const heelTipBack = {
        x: heelBack.x - 5.8,
        y: bottomY
    };
    const heelTip = {
        x: heelTipBack.x - this.heelTipWidth * 0.5,
        y: bottomY
    };
    heelTip.cp1 = {x: heelTip.x, y: archTip.y};

    let stilettoHeelBack = extractPoint(heelBack);
    stilettoHeelBack.cp1 = simpleQuadratic(heelTipBack, stilettoHeelBack, 0.7, this.backHeelDeflection);

    const rev = reverseDrawPoint(sp.right.p1, sp.right.p2);
    archTip.cp1 = rev.cp1;
    archTip.cp2 = rev.cp2;

    // shift slightly so we don't see line separating them
    const epsilon = 0.4;
    archTip = adjust(archTip, 0, epsilon);
    stilettoHeelBack = adjust(stilettoHeelBack, 0, epsilon);

    return {archTip, heelTip, heelTipBack, stilettoHeelBack};
}

function renderLock(ctx, topPoint, size = 1, rotation = 0, halfWidth = 2, headStroke = "#D3D3D3", bodyFill = "gold") {
    // head of the lock
    const left = adjust(topPoint, -halfWidth, -2);
    const right = adjust(topPoint, halfWidth, -2);
    right.cp1 = {x: left.x, y: left.y + 5};
    right.cp2 = {x: right.x, y: right.y + 5};

    // body of the lock
    const body = {};
    body.topLeft = adjust(left, -1, 1);
    body.topRight = adjust(extractPoint(right), 1, 1);
    body.right = adjust(body.topRight, 0, -5);
    body.left = adjust(body.topLeft, 0, -5);

    scalePoints(topPoint, size, left, right, body.topLeft, body.topRight, body.left, body.right);
    rotatePoints(topPoint, rotation, left, right, body.topLeft, body.topRight, body.left, body.right);

    ctx.beginPath();
    ctx.fillStyle = bodyFill;
    drawPoints(ctx, body.topLeft, body.topRight, body.right, body.left, body.topLeft);
    ctx.fill();

    ctx.beginPath();
    ctx.strokeStyle = headStroke;
    ctx.lineWidth = size;
    ctx.lineCap = "round";

    drawPoints(ctx, left, right);
    ctx.stroke();
}

export class StrapLockSidePart extends ShoeSidePart {
    constructor(...data) {
        super({lockSize: 1.0, aboveSameLayerParts: ["feet"]}, ...data);
    }

    renderShoeSidePart(ex, ctx, mods) {
        const strap = calcWideStrapSide.call(this, ex, mods);
        const lockPoint = {x: strap.left.x + 6, y: strap.left.y * 0.9 + strap.topLeft.y * 0.1};
        renderLock(ctx, lockPoint, this.lockSize, -0.1);
    }
}

export class StrapLockPart extends HeelPart {
    constructor(...data) {
        super({layer: Layer.GENITALS, lockSize: 1.0, aboveSameLayerParts: ["feet"]}, ...data);
    }

    renderClothingPoints(ex, ctx) {
        const lockPoint = {x: ex.ankle.out.x - 1, y: ex.ankle.out.y};
        renderLock(ctx, lockPoint, 0.5 * this.lockSize, 0.3, 0.8);
    }
}

export class Heels extends Clothing {
    fill() {
        return "#000";
    }

    constructor(...data) {
        super({
            clothingLayer : Clothes.Layer.MID,
            thickness     : 1,
            /**
             * How high the heel is to lift the player's height in inches
             */
            shoeHeight    : 3,
            basePointiness: 5,
            /**
             * How constrained the toes are together
             */
            shoeTightness : 10,
        }, ...data);
        // resolve dynamics between modifiers
        this.Mods = Object.assign({
            feetWidth : -this.shoeTightness,
            feetLength: 0
        }, this.Mods);
        this.Mods.feetLength += this.shoeHeight * 4;
        this.Mods.feetWidth -= this.shoeHeight;
        this.heelPointiness =
            this.basePointiness - (this.shoeHeight) * 0.5;
    }
}


export class ClosedToePumps extends Heels {
    constructor(...data) {
        super(...data);
    }

    get partPrototypes() {
        return [
            {
                side: Part.LEFT,
                Part: HeelBasePart
            },
            {
                side: Part.RIGHT,
                Part: HeelBasePart
            },
            {
                side: Part.LEFT,
                Part: HeelBaseShine
            },
            {
                side: Part.RIGHT,
                Part: HeelSideBaseClosedPart
            },
            {
                side: Part.RIGHT,
                Part: StilettoPart
            },
            {
                side: Part.RIGHT,
                Part: PlatformSidePart
            },
        ];
    }
}

export class ClosedToeStrappedPumps extends Heels {
    constructor(...data) {
        super({
            strapWidth: 1.5,
        }, ...data);
    }

    get partPrototypes() {
        return [
            {
                side: Part.LEFT,
                Part: HeelBasePart
            },
            {
                side: Part.RIGHT,
                Part: HeelBasePart
            },
            {
                side: Part.LEFT,
                Part: HeelStrapPart
            },
            {
                side: Part.RIGHT,
                Part: HeelStrapPart
            },
            {
                side: Part.LEFT,
                Part: HeelBaseShine
            },
            {
                side: Part.RIGHT,
                Part: HeelSideBaseClosedPart
            },
            {
                side: Part.RIGHT,
                Part: StilettoPart
            },
            {
                side: Part.RIGHT,
                Part: HeelSideSimpleStrapPart
            },
            {
                side: Part.RIGHT,
                Part: PlatformSidePart
            },
        ];
    }
}

export class CoveredFancyStilettos extends Heels {
    constructor(...data) {
        super(...data);
    }

    get partPrototypes() {
        return [
            {
                side: Part.LEFT,
                Part: CoveredHeelBasePart
            },
            {
                side: Part.RIGHT,
                Part: CoveredHeelBasePart
            },
            {
                side: Part.LEFT,
                Part: HeelStrapPart
            },
            {
                side: Part.RIGHT,
                Part: HeelStrapPart
            },
            {
                side: Part.LEFT,
                Part: HeelBaseShine
            },
            {
                side: Part.RIGHT,
                Part: CoveredHeelSideBasePart
            },
            {
                side: Part.RIGHT,
                Part: HeelSideWideStrapPart
            },
            {
                side: Part.RIGHT,
                Part: StilettoPart
            },
            {
                side: Part.RIGHT,
                Part: PlatformSidePart
            },
        ];
    }
}

const lockedStrappyHeelPartPrototypes = [
    {
        side: Part.RIGHT,
        Part: StrapLockSidePart
    },
    {
        side: Part.LEFT,
        Part: StrapLockPart
    },
    {
        side: Part.RIGHT,
        Part: StrapLockPart
    },
];

export class LockedCoveredFancyStilettos extends CoveredFancyStilettos {
    constructor(...data) {
        super(...data);
    }

    get partPrototypes() {
        return super.partPrototypes.concat(lockedStrappyHeelPartPrototypes);
    }
}