import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Underwear, calcBra} from "./underwear";
import {seamWidth, Layer} from "../util/canvas";
import {setStrokeAndFill} from "../util/draw";
import {
    simpleQuadratic,
    drawPoints,
    extractPoint,
    splitCurve,
    clone,
    adjust,
    breakPoint,
    interpolateCurve,
} from "drawpoint/dist-esm";

import {
    lineLineIntersection,
    pointLineIntersection,
    cartesian2polar,
    perpendicularPoint,
} from "../util/auxiliary";

import {calcBelt} from "./accessory";

export function calcSuperBra(ex) {
    //from calcBra
    if (ex.hasOwnProperty("breast") === false) {
        return null;
    }
    const sp = splitCurve(0.7, ex.breast.top, ex.breast.tip);
    const bra = {
        out: clone(extractPoint(sp.right.p1)),
        tip: clone(extractPoint(sp.right.p2))
    };
    bra.top = {
        x: ex.breast.bot.x,
        y: bra.out.y + 2
    };

    //from BraPart
    bra.out.cp1 = simpleQuadratic(bra.top, bra.out, 0.4, 1);
    bra.top.cp1 = simpleQuadratic(ex.breast.cleavage, bra.top, 0.6, 2);


    //wip
    bra.bot = adjust(ex.breast.bot, 0, 0);
    bra.cleavage = adjust(ex.breast.cleavage, 0, 0);
    bra.inner = adjust(ex.breast.in, 0, 0);

    return bra;
}

export function calcSuperBraStrap(ex) {
    //BOTTOM STRAP
    const bra = calcBra(ex);
    let strap = {};
    strap.out = {
        x: ex.breast.bot.x,
        y: ex.breast.cleavage.y
    };
    strap.outbot = {
        x: ex.breast.bot.x,
        y: ex.breast.bot.y + 3 - this.botStrapWidth,
    };
    strap.bot = {
        x: -seamWidth,
        y: ex.breast.bot.y + 4 - this.botStrapWidth,
    };
    strap.mid = {
        x: -seamWidth,
        y: ex.breast.cleavage.y
    };


    //TOP STRAP
    let sp = splitCurve((1 - this.neckCoverage), ex.neck.cusp, ex.collarbone);
    strap.strapTop = sp.left.p2;
    strap.strapTop.y -= (this.strapWidth / 2);

    strap.breastTop = adjust(ex.breast.top, 0.1 * this.strapWidth, 0.1 * this.strapWidth);
    strap.breastTop.cp1 = {
        x: strap.breastTop.x - 5 + this.topStrapCurveX,
        y: strap.breastTop.y + 10 + this.topStrapCurveY,
    };

    strap.breastOut = extractPoint(adjust(bra.out,-0.5*this.strapWidth,0));

    return strap;
}


export class SuperBraGenitalPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.GENITALS,
            loc       : "chest",
            reflect   : true,
            aboveParts: ["parts chest", "decorativeParts chest"],
        }, {
            strapBehindBreasts: true,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        const bra = calcSuperBra(ex);
        if (bra === null) {
            return;
        }

        const strap = calcSuperBraStrap.call(this, ex);

        //TOP STRAP
        if (this.showStrap && !this.strapBehindBreasts) {
            ctx.lineWidth = this.strapWidth; // top strap
            ctx.strokeStyle = this.highlight;
            ctx.beginPath();
            drawPoints(ctx, strap.breastTop, strap.breastOut);
            ctx.stroke();
        }

        setStrokeAndFill(ctx, {
                fill  : this.fill,
                stroke: this.stroke
            },
            ex);
        ctx.lineWidth = this.thickness;

        ctx.beginPath();
        drawPoints(ctx,
            bra.top,
            bra.out,
            bra.tip,
            bra.bot,
            bra.inner,
            bra.cleavage,
            bra.top
        );
        ctx.fill();
        ctx.stroke();

    }
}


export class SuperBraChestPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.FRONT,
            loc       : "+torso",
            reflect   : true,
            aboveParts: ["parts torso"],
            belowParts: ["parts chest"]
            //aboveParts: ["parts chest", "decorativeParts chest"],
        }, ...data);
    }


    renderClothingPoints(ex, ctx) {
        const bra = calcSuperBra(ex);
        if (bra === null) {
            return;
        }
        const strap = calcSuperBraStrap.call(this, ex);

        setStrokeAndFill(ctx, {
                fill  : this.fill,
                stroke: this.stroke
            },
            ex);
        ctx.lineWidth = this.thickness;
        ctx.lineCap = "round";

        //BOTTOM STRAP
        ctx.beginPath();
        drawPoints(ctx, strap.out, strap.outbot, strap.bot, strap.mid, strap.out);
        ctx.fill();

        //TOP STRAP
        if (this.showStrap) {
            ctx.lineWidth = this.strapWidth; // top strap
            ctx.strokeStyle = this.highlight;
            ctx.beginPath();
            drawPoints(ctx, strap.strapTop, strap.breastTop);
            ctx.stroke();
        }
    }
}


export function calcPanties(ex) {
    let temp;
    const panties = {};

    //top always above bot
    if (this.waistCoverageLower > this.waistCoverage) {
        this.waistCoverageLower = this.waistCoverage;
    }

    if (this.waistCoverage > 0 && this.waistCoverageLower > 0) {
        temp = splitCurve((1 - this.waistCoverage), ex.waist, ex.hip);
        panties.sideTop = temp.left.p2;
        let hip = temp.right.p2;

        let waCo = (this.waistCoverage - this.waistCoverageLower) / this.waistCoverage;
        temp = splitCurve(waCo, panties.sideTop, hip);
        panties.sideBot = temp.left.p2;
        panties.sideTop = extractPoint(panties.sideTop);

    } else if (this.waistCoverage < 0 && this.waistCoverageLower < 0) {
        temp = splitCurve(Math.abs(this.waistCoverage), ex.hip, ex.thigh.out);
        panties.sideTop = temp.left.p2;
        let thigh = temp.right.p2;

        let waCo = Math.abs(((1 + this.waistCoverage) - (1 + this.waistCoverageLower)) /
                            (1 + this.waistCoverage));
        temp = splitCurve(waCo, panties.sideTop, thigh);
        panties.sideBot = temp.left.p2;
        panties.sideTop = extractPoint(panties.sideTop);
    } else {
        temp = splitCurve((1 - this.waistCoverage), ex.waist, ex.hip);
        panties.sideTop = extractPoint(temp.left.p2);

        panties.hip = temp.right.p2;

        temp = splitCurve(Math.abs(this.waistCoverageLower), ex.hip, ex.thigh.out);
        panties.sideBot = temp.left.p2;
    }

    //top & curve
    panties.top = {
        x: -0.1,
        y: (panties.sideTop.y + this.topY - 8)
    };

    panties.sideTop.cp1 = {
        x: (panties.sideTop.x * 0.5) + (panties.top.x * 0.5),
        y: panties.sideTop.y,
    };
    panties.sideTop.cp1.x += this.curveTopX;
    panties.sideTop.cp1.y += this.curveTopY - 9;

    //bottom
    panties.bot = adjust(clone(ex.groin), -0.1, 0);

    temp = splitCurve(this.genCoverage, ex.groin, extractPoint(ex.thigh.top));
    panties.botOut = extractPoint(temp.left.p2);

    //bottom curve
    temp = splitCurve(0.5, panties.sideBot, panties.botOut);
    panties.botOut.cp1 = extractPoint(temp.left.p2);
    panties.botOut.cp1.x += this.curveBotX - 9;
    panties.botOut.cp1.y += this.curveBotY + 5;

    return panties;


}

export function calcPanties2(ex) {
    const panties = calcPanties.call(this, ex);

    //circle
    //let radius = 3.6;
    panties.center = {
        x: -0.1,
        y: panties.top.y - (2 * this.radius)
    };
    panties.top.cp1 = {
        x: this.radius * 2,
        y: panties.top.y - this.radius
    };

    //side circle
    let radius = cartesian2polar(panties.sideTop, panties.sideBot).theta;
    panties.sideBot.cp1 = perpendicularPoint(panties.sideTop, panties.sideBot, 0.5, -2 * radius);
    panties.sideBot.cp = void 0;

    return panties;


}


export class SuperPantiesPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : "groin",
            reflect            : true,
            aboveParts         : ["parts groin", "parts torso", "parts leg"],
            belowSameLayerParts: ["clothingParts leg"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        if (ex.hasOwnProperty("groin") === false) {
            return;
        }

        const panties = calcPanties.call(this, ex);

        Clothes.simpleStrokeFill(ctx, ex, this);
        ctx.beginPath();
        drawPoints(ctx,
            panties.top,
            panties.sideTop,
            panties.hip,
            panties.sideBot,
            panties.botOut,
            panties.bot
            // panties.top
        );
        ctx.fill();
        /*
        ctx.beginPath();
        drawPoints(ctx,
            panties.top,
            panties.sideTop,
            panties.sideBot,
            panties.botOut,
            panties.bot,
           //panties.top
        );
        */
        ctx.stroke();


        if (this.bow) {
            const center = adjust(panties.top, 0, -2);

            let topIn = adjust(center, 0, 1);
            let botIn = adjust(center, 0, -1);

            let topOut = adjust(center, 3, 2);
            let botOut = adjust(center, 3, -2);

            ctx.fillStyle = da.adjustColor(ctx.fillStyle,
                {s: -10,
                    l: -15
                });

            ctx.beginPath();
            drawPoints(ctx,
                topIn,
                topOut,
                botOut,
                botIn
            );
            ctx.fill();
            ctx.stroke();
        }

    }
}


export class ChastityBeltPart extends ClothingPart {
    constructor(...data) {
        super({
                layer              : Layer.FRONT,
                loc                : "groin",
                reflect            : true,
                aboveParts         : [
                    "parts groin",
                    "parts torso",
                    "parts leg",
                    "parts torso",
                    "decorativeParts torso"
                ],
                belowSameLayerParts: ["clothingParts leg"],
            }, {
                waistCoverage: 0.72,
                beltWidth    : 6,
                beltCurve    : -2,
                highlight    : "#cdc331",

                coverage: 0.2,
            },
            ...data);
    }

    renderClothingPoints(ex, ctx) {
        Clothes.simpleStrokeFill(ctx, ex, this);

        const {inTop, outTop, outMid, outBot, inBot} = calcBelt.call(this, ex);

        ctx.beginPath();
        drawPoints(ctx,
            inTop,
            outTop,
            outMid,
            outBot,
            inBot
        );
        ctx.fill();
        ctx.stroke();


        let temp = splitCurve(this.coverage, inTop, outTop);
        const top = temp.left.p2;

        drawPoints(ctx,
            inTop,
            top,
            ex.vagina.side,
            ex.groin
        );
        ctx.fill();
        ctx.stroke();


    }
}

export class SuperPanties2Part extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : "groin",
            reflect            : true,
            aboveParts         : ["parts groin", "parts torso", "parts leg"],
            belowSameLayerParts: ["clothingParts leg"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        if (ex.hasOwnProperty("groin") === false) {
            return;
        }

        const panties = calcPanties2.call(this, ex);
        Clothes.simpleStrokeFill(ctx, ex, this);
        ctx.beginPath();
        drawPoints(ctx,
            panties.center,
            panties.top,
            panties.sideTop,
            //	panties.hip,
            panties.sideBot,
            panties.botOut,
            panties.bot
            // panties.top
        );
        ctx.fill();
        /*
        ctx.beginPath();
        drawPoints(ctx,
            panties.top,
            panties.sideTop,
            panties.sideBot,
            panties.botOut,
            panties.bot,
           //panties.top
        );
        */
        ctx.stroke();
    }
}

/*

*/


export class SuperBra extends Underwear {
    constructor(...data) {
        super({
            showStrap     : false,
            strapWidth    : 1.5,
            neckCoverage  : 0.7,
            thickness     : 0.5,
            botStrapWidth : 0,
            topStrapCurveX: 0,
            topStrapCurveY: 0,
            highlight     : "hsl(346, 50%, 70%)",
        }, ...data);
    }

    /*
    stroke() {
        return "hsl(346, 50%, 70%)";
    }
    */

    fill() {
        return "hsl(346, 57%, 82%)";
    }

    get partPrototypes() {
        return [

            {
                side: null,
                Part: SuperBraChestPart
            },
            {
                side: null,
                Part: SuperBraGenitalPart
            }
        ];
    }
}

export class SuperPanties extends Underwear {
    constructor(...data) {
        super({
            waistCoverage     : 0.11,
            waistCoverageLower: -0.05,
            genCoverage       : 1,
            topY              : 0,
            curveTopX         : 0,
            curveTopY         : 0,
            curveBotX         : 0,
            curveBotY         : 0,
            bow               : true,
        }, ...data);
    }

    fill() {
        return "hsl(346, 57%, 82%)";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: SuperPantiesPart
            }
        ];
    }
}

export class BikiniBottom extends Underwear {
    constructor(...data) {
        super({
            waistCoverage     : 0.11,
            waistCoverageLower: -0.05,
            genCoverage       : 1,
            topY              : 0,
            curveTopX         : 0,
            curveTopY         : 0,
            curveBotX         : 0,
            curveBotY         : 0,
            radius            : 2.8,
            thickness         : 0.6,
        }, ...data);
    }

    stroke() {
        return "hsla(335, 800%, 30%, 1)";
    }

    fill() {
        return "hsla(335, 100%, 42%, 1)";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: SuperPanties2Part
            }
        ];
    }
}

export class ChastityBelt extends Underwear {
    constructor(...data) {
        super({}, ...data);
    }

    fill() {
        return "#50a9cc";
    }

    stroke() {
        return "#343570";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: ChastityBeltPart
            }
        ];
    }
}