import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Layer} from "../util/canvas";
import {setStrokeAndFill} from "../util/draw";
import {
    simpleQuadratic,
    drawPoints,
    getPointOnCurve,
    adjust,
    extractPoint,
    tracePoint,
	interpolateCurve,
	splitCurve,
} from "drawpoint/dist-esm";
import {averagePoint} from "..";

import {Hat} from "./hats";

//TODO - shading
export class MagicHatFrontPart extends ClothingPart {
    constructor(...data) {
        super({
                layer: Layer.ABOVE_HAIR,
                loc  : "+head",
				reflect: true,
            },
            {
            },
            ...data);
    }

    renderClothingPoints(ex, ctx) {
        const {center,inBot,out,inTop,coneBase,coneTop,forhead} = calcMagicHat.call(this, ex);
        Clothes.simpleStrokeFill(ctx, ex, this);
		
		//Cone
		ctx.beginPath();
        drawPoints(ctx,
			coneBase,
			coneTop,
			 extractPoint(inTop)
		);
        ctx.fill();
		
        ctx.beginPath();
        drawPoints(ctx,
			coneBase,
			coneTop,
		);
        ctx.stroke();
		
		//Brim
		ctx.beginPath();
        drawPoints(ctx,
			out,
			inTop,
			forhead,
			coneBase,	
		);
		ctx.fill();
	
	//the bottom curve of the cone
	if(inBot.y>inTop.y){	
		ctx.beginPath();
        drawPoints(ctx,
			forhead,
			coneBase,
			
		);
        ctx.stroke();
	};
	
		ctx.beginPath();
        drawPoints(ctx,
			out,
			inTop,
		);
		//ctx.fill();
        ctx.stroke();

    }
}

export class MagicHatBackPart extends ClothingPart {
    constructor(...data) {
        super({
                layer: Layer.BACK,
                loc  : "+head",
				reflect: true,
            },
            {
            },
            ...data);
    }

    renderClothingPoints(ex, ctx) {
        const {center,inBot,out,inTop,coneBase,coneTop,forhead} = calcMagicHat.call(this, ex);
        Clothes.simpleStrokeFill(ctx, ex, this);
	 
		ctx.beginPath();
        drawPoints(ctx,
            inBot,
			out,
			inTop
			
		);
        ctx.fill();
        ctx.stroke();

    }
}





export function calcMagicHat(ex) {
    const center = {x:-0.2, y:ex.skull.side.y+3+this.centerOffset}
	let inTop = adjust(center, 0, 6+this.brimAngle);
	let inBot = adjust(center, 0, -6-this.brimAngle);
	let out = adjust(center, 36+this.brimWidth, 0);		
	
	let forhead = adjust(center, 0, (6+this.brimAngle) / 2 );
	
	out.cp1 = {
		x: (out.x + inBot.x) * 0.5,
		y: inBot.y 
	};
		
	inTop.cp1 = {
		x: (inTop.x + out.x) * 0.5,
		y: inTop.y 
	};
	
	let rim = interpolateCurve(ex.skull,ex.skull.side,{x:null, y:center.y}) 
	 
	//let temp = splitCurve(0.5+this.coneWidth/10,out,inTop) 
	let coneBase = rim[0]//temp.left.p2;
	
	coneBase = adjust(coneBase,this.coneWidth+2,0)
	
	coneBase.cp1 = {
		x: (coneBase.x + forhead.x) * 0.5,
		y: forhead.y 
	};
	
	let coneTop = adjust(center, 0, 45+this.coneHeight)
    return {
		center,
        inBot,
		out,
		inTop,
		coneBase,
		coneTop,
		forhead
    };
}


export class MaidHeadpiecePart extends ClothingPart {
    constructor(...data) {
        super({
                layer: Layer.ABOVE_HAIR,
                loc  : "head",
				reflect: true,
            },
            {
            },
            ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		let inBot = {
			x: -0.2,
			y: ex.skull.y - 5 + this.centerOffset
		}
		
		let inTop = {
			x: -0.2,
			y: inBot.y + this.height
		}
		 
		let outTop = {
			x: inTop.x + this.width,
			y: inTop.y - this.drop
		}//adjust(ex.skull.side,10,0);
		
		outTop.cp1 = {
			x: 0.5 * (inTop.x + outTop.x),
			y: inTop.y
		};
		
		let a = Math.sqrt( Math.pow(this.height,2)/2 ); //pythagoras
		let outBot = {
			x: outTop.x - a,
			y: outTop.y - a
		};
		
		inBot.cp1 = {
			x: 0.5 * (inBot.x + outBot.x),
			y: inBot.y
		};
		
		ctx.beginPath();
        drawPoints(ctx,
			inTop,
			outTop,
			outBot,
			inBot
			
		);
        ctx.fill();
        ctx.stroke();
		
    }
}


/*

*/

export class MagicHat extends Hat {
    constructor(...data) {
        super({
			centerOffset: 0,			
			brimWidth: 0,
			brimAngle: 0,
			coneHeight: 0,
			coneWidth: 0,
		},...data);
    }

	fill() {
        return "#343232";
    }
	
	stroke() {
       return "#201b1b";
    }
	
    get partPrototypes() {
        return [
            {
                side: null,
                Part: MagicHatFrontPart
            },{
                side: null,
                Part: MagicHatBackPart
            }
        ];
    }
}

export class MaidHeadpiece extends Hat {
    constructor(...data) {
        super({
			centerOffset: 2,
			height: 9,
			width: 12,
			drop: 4,
		},...data);
    }

	fill() {
        return "white";
    }
	
	stroke() {
       return "black";
    }
	
    get partPrototypes() {
        return [
            {
                side: null,
                Part:  MaidHeadpiecePart
            }
        ];
    }
}