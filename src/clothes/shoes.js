import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Part} from "../parts/part";
import {Layer} from "../util/canvas";
import {ShadingPart} from "../draw/shading_part";
import {
    simpleQuadratic,
    drawPoints,
    extractPoint,
    clone,
    splitCurve,
} from "drawpoint/dist-esm";

export class LeftBaseShoeShading extends ShadingPart {
    constructor(...data) {
        super({
            loc  : "left feet",
            layer: Layer.FRONT,
        }, ...data);
    }

    calcDrawPoints(ex) {
        const toeBox = ex.toe.toebox;

        const sp = splitCurve(0.5, ex.toe.out, ex.toe.in);
        const inner = sp.right.p2;
        const bot = sp.left.p2;
        bot.cp1 = simpleQuadratic(toeBox, bot, 0.5, 1);

        const ankleInBot = clone(ex.ankle.inbot);
        toeBox.cp1 = {
            x: ankleInBot.x - 1,
            y: ankleInBot.y - 5
        };
        toeBox.cp2 = {
            x: toeBox.x - 3,
            y: toeBox.y
        };

        return [toeBox, bot, inner, ankleInBot, toeBox];

    }
}


export class RightBaseShoeShading extends ShadingPart {
    constructor(...data) {
        super({
            loc  : "right feet",
            layer: Layer.FRONT,
        }, ...data);
    }

    calcDrawPoints(ex) {
        const toeBox = ex.toe.toebox;

        const sp = splitCurve(0.8, ex.toe.out, ex.toe.in);
        const bot = sp.left.p2;
        bot.cp1 = simpleQuadratic(toeBox, bot, 0.5, -1);

        const ankleOutBot = extractPoint(ex.ankle.outbot);
        ankleOutBot.cp1 = simpleQuadratic(bot, ankleOutBot, 0.5, -10);

        toeBox.cp1 = simpleQuadratic(ankleOutBot, toeBox, 0.7, 3);

        return [ankleOutBot, toeBox, bot, ankleOutBot];

    }
}


export class ShoeSidePart extends ClothingPart {
    constructor(...data) {
        super({
            layer: Layer.BACK,
            loc  : "feet",
        }, ...data);
    }
}


export class ShoePart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.FRONT,
            loc       : "feet",
            aboveParts: ["parts feet", "parts leg"],
            belowParts: ["shadingParts feet"],
        }, ...data);
    }
}


export class ShoeBasePart extends ShoePart {
    constructor(...data) {
        super({
            layer       : Layer.FRONT,
            loc         : "feet",
            shadingParts: [LeftBaseShoeShading, RightBaseShoeShading]
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {

        ex.toe.toebox = {
            x: ex.toe.center.x,
            y: ex.toe.center.y + this.toeHeight
        };

        const outTop = extractPoint(ex.ankle.out);
        const out = ex.ankle.outbot;
        const outBot = ex.toe.out;
        const inBot = ex.toe.in;
        const inner = clone(ex.ankle.inbot);
        const inTop = ex.ankle.in;

        inner.cp2.x -= 2;
        outTop.cp1 = simpleQuadratic(inTop, outTop, 0.5, this.tongueDeflection);

        // scale to simulate clothing thickness
        Clothes.simpleStrokeFill(ctx, ex, this);


        // top strap
        ctx.beginPath();
        drawPoints(ctx,
            outTop, out, outBot, inBot, inner, inTop, outTop
        );
        ctx.fill();
        ctx.stroke();

    }
}


export class Shoe extends Clothing {
    constructor(...data) {
        super({
            clothingLayer   : Clothes.Layer.MID,
            /**
             * How high the heel is to lift the player's height
             */
            shoeHeight      : 0,
            /**
             * How high the tongue of the shoe (assuming it has one) goes up
             */
            tongueDeflection: 2,
            /**
             * How high the toe-box of the shoe is
             */
            toeHeight       : 0,
            Mods            : {
                feetLength: -10,
                feetWidth : -2
            }
        }, ...data);
    }
}


export class FlatShoes extends Shoe {
    constructor(...data) {
        super({
            shoeHeight: 0,
        }, ...data);
    }

    stroke() {
        return "#000";
    }

    fill() {
        return "hsl(0,10%,20%)";
    }

    get partPrototypes() {
        return [
            {
                side: Part.LEFT,
                Part: ShoeBasePart
            },
            {
                side: Part.RIGHT,
                Part: ShoeBasePart
            },
        ];
    }
}


