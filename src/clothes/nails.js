import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Layer} from "../util/canvas";
import {
    drawPoints,
    adjust,
    extractPoint,
	splitCurve,
} from "drawpoint/dist-esm";
import {Location, setStrokeAndFill} from "..";
import {Part} from "../parts/part";

import {
	perpendicularPoint,
} from "../util/auxiliary";


export class ThumbPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.ARMS,
            loc       : "+hand",
            aboveParts: ["parts arm", "decorativeParts arm", "parts hand"],
        }, {
 
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
			
		let tip = ex.thumb.tip;
		let out = ex.thumb.out;
		let temp;
				
		temp = da.splitCurve(-0.08,tip,out);
		let bottom = extractPoint(temp.left.p2);
					
		temp = da.splitCurve(0.15,tip,out);
		let top = extractPoint(temp.left.p2);
				
		top.x += 0.2;
				
		bottom.cp1 = perpendicularPoint(top,bottom,0.7,0.8);
		top.cp1 = perpendicularPoint(top,bottom,0.7,-1.0);
			
		ctx.beginPath();
		drawPoints(ctx,top,bottom,top);
		ctx.fill();
    }
}


export class FingerPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.ARMS,
            loc       : "+hand",
            aboveParts: ["parts arm", "decorativeParts arm", "parts hand"],
        }, {
 
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
			
		let tip = ex.hand.tip;
		let out = ex.hand.knuckle;
		let temp;
		
		temp = splitCurve(-0.08,tip,out);
		let bottom = extractPoint(temp.left.p2);
						
		temp = splitCurve(0.15,tip,out);
		let top = extractPoint(temp.left.p2);
		
		top.x -= 0.2;
		bottom.x -= 0.6;
		
		bottom.cp1 = perpendicularPoint(top,bottom,0.7,0.8);
		top.cp1 = perpendicularPoint(top,bottom,0.7,-1.0);
		

		ctx.beginPath();
		drawPoints(ctx,top,bottom,top);
		ctx.fill();
    }
}


export class BodyMakeup extends Clothing {
    constructor(...data) {
        super({
            clothingLayer: Clothes.Layer.BASE,
        }, ...data);
    }
	
	fill(){
        return "hsla(0, 0%, 28%, 1)";
    }
}

export class Nails extends BodyMakeup {
    constructor(...data){
        super({
			clothingLayer: Clothes.Layer.BASE,
		},...data);
    }

	fill(){
        return "#b91100";
    }
	
    get partPrototypes(){
        return [
           {
                side: Part.LEFT,
                Part: FingerPart,
			},{
				side: Part.LEFT,
                Part: ThumbPart,
            },{
                side: Part.RIGHT,
                Part: FingerPart,
			},{
				side: Part.RIGHT,
                Part: ThumbPart,
            } 	
        ];
    }
}
