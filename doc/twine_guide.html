---
layout: page
title: Twine Integration Guide
permalink: /twine_guide.html
group: Twine
css: syntax
---
<div class="toc">
<a class="toc-link toch2" href="#requirements">Requirements</a>
<a class="toc-link toch2" href="#recommended_setup">Recommended setup</a>
<a class="toc-link toch2" href="#components">Components</a>
<a class="toc-link toch2" href="#import">Import</a>
<a class="toc-link toch2" href="#before_loading">Before Loading</a>
<a class="toc-link toch2" href="#after_loading">After Loading</a>
<a class="toc-link toch2" href="#outside_loading">Outside Loading</a>
<p class="toc-caption"></p>
<p class="toc-toggle">toggle TOC (ctrl + &#8660;)</p>
</div>
<p>This guide is for integrating DAD into a Twine game (with the SugarCube 2
story format). You should read this while going through the <a href="dist/twine_demo.html"><strong>demo</strong></a>
(download it and Import From File inside Twine).
For more general tips, refer to the <a href="usage.1.0.html">usage guide</a>.</p>
<p>A caveat is that I'm not a SugarCube expert, so there are probably better ways to do some things.
Take a look at <a href="https://qjzhvmqlzvoo5lqnrvuhmg-on.drv.tw/UInv/Sample_Code.html#Main%20Menu">Twine sample code</a>
and the <a href="https://www.motoslave.net/sugarcube/2/docs/">SugarCube reference</a> for help.</p>
<h2 class="anchor">Requirements <a class="anchor-link" title="permalink to section" href="#requirements"
                               name="requirements">&para;</a></h2>
<ul>
<li>SugarCube version 2.31.x or newer </li>
<li>DAD version 1.28.x or newer.</li>
</ul>
<h2 class="anchor">Recommended setup <a class="anchor-link" title="permalink to section" href="#recommended_setup"
                               name="recommended_setup">&para;</a></h2>
<p>You can either work inside Twine 2 (the app), or compile the story from external files using
Tweego. I recommend the second option for reasonably sized projects
because the text editor inside Twine is not great…
You might resort to doing the editing in something else (e.g. WebStorm, sublime) and copying
it back to test, but this makes debugging slow. </p>
<p>However I will explain things assuming you are working inside the Twine app since it's the
easiest way for you to get started, and there aren't that many differences.</p>
<h2 class="anchor">Components <a class="anchor-link" title="permalink to section" href="#components"
                               name="components">&para;</a></h2>
<p>There are 2 major components of interactivity to a Twine game</p>
<ul>
<li>Story JS<ul>
<li>runs at the start of the story</li>
<li>defined at the start in a special place</li>
<li>can create macros for use inside TwineScript</li>
<li>has to access the state through the <code>State</code> <a href="https://www.motoslave.net/sugarcube/2/docs/#state-api">API</a> 
like <code>State.variables.PC</code></li></ul></li>
<li>TwineScript<ul>
<li>runs when you play the passage</li>
<li>embedded in the passages</li>
<li>can only use <a href="https://www.motoslave.net/sugarcube/2/docs/#macros">macros</a>, but <code>&lt;&lt;run {pure JS}&gt;&gt;</code>
and <code>&lt;&lt;script&gt;&gt;{pure JS}&lt;&lt;/script&gt;&gt;</code> allows you to run pure JS</li>
<li>can access the state directly through special syntax like <code>$PC</code></li></ul></li>
</ul>
<p>Unless something is easier to do in TwineScript, I recommend implementing it in JS
because it could be reusable outside of your story, or if you decide to switch story formats.</p>
<h2 class="anchor">Import <a class="anchor-link" title="permalink to section" href="#import"
                               name="import">&para;</a></h2>
<p>The first thing is to import the <code>da</code> module. 
The simplest method is through the <code>importScripts</code> function near 
the start of your Story JS after including other external libraries like:</p>

{% highlight javascript %}setup.lockID = LoadScreen.lock();
importScripts("http://perplexedpeach.gitlab.io/dynamic-avatar-drawer/dist/da.js")
    .then(function () {
        setup.da = da;
        // extend the da module here

        da.load().then(function () {
            // da module is ready to draw here; you can create/manage drawing canvases here

            Engine.play("StoryInit", true);
            Engine.play(passage(), true);
            LoadScreen.unlock(setup.lockID);
        });
    }).catch(function (error) {
        alert(error);
    }
);
{% endhighlight %}

<p>This has the advantage of always pointing to the latest version of DAD, but you may not want that.
If you want a specific version, you can download <code>dist/da.js</code> from the 
<a href="https://gitlab.com/PerplexedPeach/dynamic-avatar-drawer">repository</a> by searching 
the history for the commit, like "1.29.1". 
You would then copy the content of that to the top of story javascript.
Note that the <code>da</code> module is placed in the global frame so you can refer to it in TwineScript like
<code>&lt;&lt;run da.{...}&gt;&gt;</code>.</p>
<p>Alternatively, you can clone the DAD repository (or as a submodule) and either import the latest
<code>dist/da.js</code>, or directly from source as modules such as
<code>import {...} from "dynamic-avatar-drawer/src/player/player.js"</code>
and bundle everything together with <a href="https://webpack.js.org/">webpack</a>.
This has the advantage of allowing an IDE (like WebStorm) to make coding a lot easier.</p>
<p>I recommend you create assets directly as modules inside <code>src/clothes</code>. This allows you
to make pull requests to share your templates with other people.</p>
<aside class='notice'><h4>Note</h4>DAD is licensed under LGPL so you should try to make your DAD-related code easily accessible.
(the easiest would be to submit pull requests bringing your stuff back into the main project)</aside>
<h2 class="anchor">Before Loading <a class="anchor-link" title="permalink to section" href="#before_loading"
                               name="before_loading">&para;</a></h2>
<p>Code extending the library should be placed before <code>da.load()</code>. As seen above, it should be
placed at <code>// extend the da module here</code>. See the <a href="extension.html">extension guide</a> and
the demo's code for what to do. Patterns can also be defined here as</p>

{% highlight javascript %}    addPattern("red plaid",
        "http://www.creattor.com/files/37/1681/plaid-fabrics-textures-screenshots-1.jpg");
{% endhighlight %}

<p>Which you can later specify as the color for <code>fill</code>, <code>stroke</code>, and similar properties like</p>

{% highlight javascript %}            var belt = da.Clothes.create(da.SimpleBelt, {
                fill: da.getPattern("red plaid", 100),
            });
            State.variables.PC.wearClothing(belt);
{% endhighlight %}

<p>Which would translate to TwineScript inside a passage like</p>
<pre><code>&lt;&lt;set _belt = da.Clothes.create(da.SimpleBelt, {
                              fill: da.getPattern("red plaid", 100),
                          });&gt;&gt;
&lt;&lt;run $PC.wearClothing(_belt)&gt;&gt;
</code></pre>
<h2 class="anchor">After Loading <a class="anchor-link" title="permalink to section" href="#after_loading"
                               name="after_loading">&para;</a></h2>
<p>We can now get canvas groups for drawing. In the demo, properties of drawing the PC is stored
in <code>settings</code>, which is saved across sessions, but you can put it somehwere else.
You might also want to look at <a href="usage.1.0.html#player#drawfocus">focused windows</a> for drawing
high resolution portraits for dialogs.</p>
<h2 class="anchor">Outside Loading <a class="anchor-link" title="permalink to section" href="#outside_loading"
                               name="outside_loading">&para;</a></h2>
<p>You can place methods here that don't depend on immediately having access to <code>da</code> properties.
The most standard function would be something like <code>drawToMenu(PC)</code> in the demo. These functions
have to be attached to <code>window</code> to be usable inside passages, so define them as</p>

{% highlight javascript %}window.drawToMenu = function(avatar) {/* ... */};
{% endhighlight %}
