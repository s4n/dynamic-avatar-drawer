---
layout: post
title:  "1.0 Full Release"
date:   2016-09-04
categories: release
---
Finally the wait for v1.0 is over. It's a complete rewrite of v0.13.
Many clothing assets weren't adapted over, and will have to be recreated
(but should be easier now with the new system).

Check the [reference]({{site.baseurl}}/out/index.html) for detailed documentation and summary of features.
The [usage guide]({{site.baseurl}}/usage.1.0.html) is now updated for the current version.