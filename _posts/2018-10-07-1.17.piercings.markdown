---
layout: post
title:  "1.17 Piercings"
date:   2018-10-07
categories: release
---

- new clothing category: jewelry (and piercing subcategory)
    - ring
    - stud
    - chain
- fixed saving/loading
- made ears individual to a side

## Piercings

![Nominal piercings](https://i.imgur.com/3VU8NzY.png)

Piercings are similar to tattoos in that they can be placed anywhere.
Currently there are ring and stud piercings, and chain jewelry, all with 
some parameters such as radius. 


## Piercing Definition

Look at the tester source code and `src/clothes/jewelry.js` to see options for piercings.
You can also get the generated code from the GUI (under clothing/Jewelry section).

### In code

{% highlight javascript %}
var chainJewelry = da.Clothes.create(da.ChainJewelry, {
    slack: 2,
    stroke: "silver",
    loc: "+nose",
    // which part group the location is expected to be on (parts, faceParts)
    requiredParts: "faceParts",
    // drawpoint relative location to start
    relativeLocation: {
        drawpoint: "nose.center",
        dy: -1.5,
        dx: 0
    },
    // drawpoint relative location to end
    endRelativeLocation: {
        drawpoint: "ear.mid",
        dy: -1.5,
        dx: 0.5,
    },
});
PC.wearClothing(chainJewelry);
{% endhighlight %}

### Options

`radius` - ring and stud piercings have this option and it configures the size of the piercing.

`rotation` - ring option that configures which half of the piercing is seen above the body part.
Essentially gives the impression of attaching the piercing at different angles (play around with
it to see what I mean).

`thickness` - ring and chain option for how wide the stroke is. Studs are filled instead of stroked
so use radius only to control size.

`slack` - chain option for how droopy the chain is. 0 is super tight - a straight line connecting
the start and end.
